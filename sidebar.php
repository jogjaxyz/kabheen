<?php
/**
 * The sidebar containing the main widget area
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Jogja
 */

if ( ! is_active_sidebar( 'kabheen-primary-sidebar' ) ) {
	return;
}
?>

<div id="secondary" class="col-md-3" role="complementary">
	<?php dynamic_sidebar( 'kabheen-primary-sidebar' ); ?>
</div><!-- #secondary -->
