<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Jogja
 */

get_header(); ?>

	<!-- START MAIN CONTENT -->
	<div id="main">
		<div class="container"> 
			<div class="row">
				<div id="primary" class="content-area col-md-9">
					<div id="content" class="site-content">

						<?php
							while ( have_posts() ) : the_post();

								get_template_part( 'template-parts/format', get_post_format() );

								the_post_navigation();

							endwhile; // End of the loop.
							
							kabheen_post_about_author(); 
							kabheen_related_post(); 
							// If comments are open or we have at least one comment, load up the comment template.
							if ( comments_open() || get_comments_number() ) :
								echo '<div class="comment-outer">';
									comments_template();
								echo '</div>';
							endif;
						 ?>

					</div>
				</div>

				<?php get_sidebar(); ?>

			</div>
		</div><!-- #main -->
	</div><!-- #primary -->

<?php get_footer();
