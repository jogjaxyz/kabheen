<?php
/**
 * Template part for displaying page content in page.php.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Kabheen
 */
?>

<div id="post-<?php the_ID(); ?>" <?php post_class( 'post' ); ?>>
	<div class="entry-format">
		<?php kabheen_entry_meta(); ?>
	</div><!-- end entry-format -->
	
	<div class="inner">
		<h1 class="entry-title">
			<?php echo single_post_title(); ?>
		</h1>
		
		<div class="entry-header">
			<?php kabheen_post_author_name(); ?>
			<?php kabheen_post_categories(); ?>
			<?php kabheen_post_comment_link(); ?>
		</div><!-- end entry-header -->
		<div class="entry-content">
			<?php
				the_content();				
				wp_link_pages( array(
					'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'kabheen' ),
					'after'  => '</div>',
				) );
			?>
		</div><!-- end entry-content -->
	</div><!-- end inner -->
</div><!-- #post-## -->
