<?php $event_details  = carbon_get_post_meta( get_the_ID(), 'kabheen_event_items', 'complex' ); ?>

 <!-- START MAIN CONTENT -->
<div id="content" class="site-content">
	<div class="container">
		<div class="row">
			<div class="story-content">
				<div class="col-md-8 col-md-offset-2">
					
					<?php if ( ! empty( $event_details ) ) : ?>
						<?php foreach ( $event_details as $event ) : ?>
							
							<div class="line-story">
								<?php $get_image_src = wp_get_attachment_image_src( get_post_thumbnail_id( $event['event'] ), 'full' ); ?>
								<?php if ( ! empty( $get_image_src ) ) : ?>
									<div class="thumb-story">
										<img src="<?php echo esc_url( $get_image_src[0] ); ?>" class="media-object">
									</div>
								<?php endif; ?>
								<div class="body-story">
									<h2><?php echo get_the_title( $event['event'] ); ?></h2>
									<?php echo get_post_field( 'post_content', $event['event'] ); ?>
								</div><!-- end media-body -->
							</div><!-- end line-story -->

						<?php endforeach; ?>
					<?php endif; ?>

				</div><!-- end col -->
			</div><!-- end story-content -->
		</div><!-- end row -->
	</div><!-- end container -->
</div><!-- end #content -->
<!-- END OF MAIN CONTENT -->