
<!-- START MAIN CONTENT -->
<div id="content" class="site-content">
	<div class="container">
		<div class="row">
			<?php get_template_part( 'template-parts/content-gallery-filters' ); ?>
		</div>
		<div class="row">
			<?php 
				$args = array(
					'post_type'				=> 'kabheen-gallery',
					'ignore_sticky_posts'	=> true,
				);
				$galleries = new WP_Query( $args );
			 ?>
			<?php if ( $galleries->have_posts() ) : ?>

				<div class="gallery-content">
					<div id="gallery-grid" class="thumbnail-wrapper">
				
						<?php while ( $galleries->have_posts() ) : $galleries->the_post(); ?>
							
							<?php $classes = get_the_terms( get_the_ID(), 'gallery_cat' ); ?>
							<?php if ( has_post_thumbnail() ) : ?>
								<figure class="mix col-md-3 col-sm-6 col-xs-12 thumb <?php foreach ( $classes as $class ) { echo esc_attr( $class->slug ) . ' '; } ?>">
									<a href="#" data-toggle="modal" data-target=".pop-up-<?php the_ID(); ?>">
										<?php the_post_thumbnail( 'medium' ); ?>
									</a>
								</figure><!-- end col -->
							<?php endif; ?>
							
							<?php ob_start(); ?>
							
								<div class="modal fade pop-up-<?php the_ID(); ?>" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel-<?php the_ID(); ?>" aria-hidden="true">
									<div class="modal-dialog modal-lg">
										<div class="modal-content">
											<div class="modal-header">
												<h4 class="modal-title" id="myLargeModalLabel-<?php the_ID(); ?>"><?php the_title(); ?></h4>
											</div>
											<div class="modal-body">
												<?php the_post_thumbnail( 'full' ); ?>
											</div>
										</div><!-- end modal content -->
									</div><!-- end modal-dialog -->
								</div><!-- end modal -->
			 
							<?php 
								$popups[] = ob_get_contents();
								ob_end_clean(); 
							?>

						<?php endwhile; ?>
						<?php wp_reset_postdata(); ?>
						<?php else: ?>
						<?php get_template_part( 'template-parts/content-none' ); ?>
				
					</div><!-- end thumbnail-wrapper -->
					
					<?php if ( ! empty( $popups ) ) : ?>
						<?php foreach ( $popups as $popup ) : ?>
							<?php echo ''.$popup; ?>
						<?php endforeach; ?>
					<?php endif; ?>

				</div><!-- end gallery-content -->
			 
			 <?php endif; ?>
		</div><!-- end row -->

	</div><!-- end container -->
</div><!-- end #content -->
<!-- END OF MAIN CONTENT -->
