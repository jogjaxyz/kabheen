
<?php 
	$section_title 	= get_theme_mod( 'kabheen_the_bride_and_groom_section_title', 'We are in love & getting married' );
	$the_bride_name = get_theme_mod( 'kabheen_the_bride_name', 'ANDIEN SULISTIAWATI' );
	$the_groom_name = get_theme_mod( 'kabheen_the_groom_name', 'JHONNY PURWODINOTO' );
 ?>
<div class="container">
	<h2 class="closing"><?php echo ''.$section_title; ?></h2>
	<div class="row">
		<div class="col-md-12">
			<div class="name-bottom">
				<p><?php echo ''.$the_bride_name; ?></p>
				<p>&amp;</p>
				<p><?php echo ''.$the_groom_name; ?></p>
			</div><!-- end name-bottom -->
		</div>
	</div>
</div><!-- end container -->