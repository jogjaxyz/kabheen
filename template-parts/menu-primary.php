<?php

/**
 * The Template for displaying menu primary
 *
 * @author 		ngaa.men
 * @version     1.0
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
?>

<div class="site-navigation navbar-collapse collapse">
	<div class="nav-menu">
		
		<?php if ( has_nav_menu( 'kabheen-primary-menu' ) ) : 

			$menu_args = array(
				'theme_location' 	=> 'kabheen-primary-menu',
				'container' 		=> false,
			);
			wp_nav_menu( $menu_args );

		else : ?>

			<ul class="menu">
				<?php wp_list_pages( array( 'depth' => 1,'sort_column' => 'menu_order','title_li' => '', 'include'  => 2 ) ) ?>
			</ul>
			
		<?php endif; ?>

	</div><!-- end nav-menu -->
</div><!-- end site-navigation -->