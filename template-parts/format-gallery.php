<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="entry-format">
		<?php kabheen_entry_meta(); ?>
	</div><!-- end entry-format -->

	<div class="inner">
		<div class="entry-media">
			<?php
				$galleries = get_post_meta( get_the_ID(), 'kabheen_format_gallery_details', true ); 

				if ( ! empty( $galleries['items'] ) ) : ?>
					
					<div class="carousel slide media-gallery" data-ride="carousel">
						<div class="carousel-inner">
							<?php 
								$galleries 	= explode( ",", $galleries['items'] ); 
								$counter 	= 1;
								foreach ( $galleries as $gallery ) { 
									$active_class 	= ( 1 == $counter ) ? 'active' : '';
									$img_src 		= wp_get_attachment_image_src( $gallery, 'full' );
									$the_image 		= kabheen_image_resize( $img_src[0], 590, 395 );
								?>
								<div class="item <?php echo esc_attr( $active_class ); ?>">
									<img src="<?php echo esc_url( $the_image ); ?>" alt="<?php esc_html_e( 'Gallery Image', 'kabheen' ); ?>">
								</div><!-- end item -->
							<?php $counter++; } ?>
						</div><!-- end carousel-inner -->

						<!-- Controls -->
						<a class="left carousel-control" href="#media-gallery" role="button" data-slide="prev">
							<span class="fa fa-fw fa-long-arrow-left"></span>
						</a>
						<a class="right carousel-control" href="#media-gallery" role="button" data-slide="next">
							<span class="fa fa-fw fa-long-arrow-right"></span>
						</a>

					</div><!-- end carousel -->

			<?php endif; ?>

		</div><!-- end entry-media -->
		<?php if ( is_single() ) : ?>
			<h1 class="entry-title">
				<?php echo single_post_title(); ?>
			</h1>
		<?php else : ?>
			<h2 class="entry-title">
				<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
			</h2>
		<?php endif; ?>
		<div class="entry-header">
			<?php kabheen_post_author_name(); ?>
			<?php kabheen_post_categories(); ?>
			<?php kabheen_post_comment_link(); ?>
		</div><!-- end entry-header -->
		<div class="entry-content">
			<?php
				the_content( sprintf(
					/* translators: %s: Name of current post. */
					wp_kses( __( 'Continue reading %s <span class="meta-nav">&rarr;</span>', 'kabheen' ), array( 'span' => array( 'class' => array() ) ) ),
					the_title( '<span class="screen-reader-text">"', '"</span>', false )
				) );
				wp_link_pages( array(
					'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'kabheen' ),
					'after'  => '</div>',
				) );
			?>
		</div><!-- end entry-content -->
	</div><!-- end inner -->
</div><!-- end post -->