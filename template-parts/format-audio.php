<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="entry-format">
		<?php kabheen_entry_meta(); ?>
	</div><!-- end entry-format -->

	<div class="inner">
		<?php $audio = hybrid_media_grabber( array( 'type' => 'audio', 'split_media' => true ) ); ?>
		<?php if ( ! empty( $audio ) ) : ?>
			<div class="entry-media">
				<?php printf( '%s', $audio ); ?>
			</div>
		<?php endif; ?>
		<?php if ( is_single() ) : ?>
			<h1 class="entry-title">
				<?php echo single_post_title(); ?>
			</h1>
		<?php else : ?>
			<h2 class="entry-title">
				<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
			</h2>
		<?php endif; ?>
		<div class="entry-header">
			<?php kabheen_post_author_name(); ?>
			<?php kabheen_post_categories(); ?>
			<?php kabheen_post_comment_link(); ?>
		</div><!-- end entry-header -->
	</div><!-- end inner -->
</div><!-- end post -->