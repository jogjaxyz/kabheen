<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="entry-format">
		<?php kabheen_entry_meta(); ?>
	</div><!-- end entry-format -->

	<div class="inner">
		<div class="entry-header">
			<?php kabheen_post_author_name(); ?>
			<?php kabheen_post_categories(); ?>
			<?php kabheen_post_comment_link(); ?>
		</div><!-- end entry-header -->
		<div class="entry-content">
			<blockquote>
				<?php if ( is_single() ) : ?>
					<?php the_content(); ?>				
				<?php else : ?>
					<?php the_excerpt(); ?>
				<?php endif; ?>
				<cite><?php the_title(); ?></cite>
			</blockquote>
			<?php 
				wp_link_pages( array(
					'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'kabheen' ),
					'after'  => '</div>',
				) ); 
			?>
		</div><!-- end entry-content -->
	</div><!-- end inner -->
</div><!-- end post -->