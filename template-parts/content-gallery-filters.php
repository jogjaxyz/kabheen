<div class="filter-box">
	<ul id="options">
		<?php $cats = get_terms( 'gallery_cat', 'hide_empty=1' ); ?>
		<li class="filter" data-filter="all" ><?php esc_html_e( 'All Photos', 'kabheen' ); ?></li>
		<?php foreach ( $cats as $cat ) { ?>
			<li class="filter" data-filter=".<?php echo esc_attr( $cat->slug ); ?>"><?php echo esc_attr( $cat->name ); ?></li>
		<?php } ?>
	</ul>	
</div><!-- end filter -->