<?php 
	if ( is_page_template( 'page-templates/page-builder.php' ) ) {
		return;
	}

	$get_header_image 	= get_header_image();
	$header_image 		= ! empty( $get_header_image ) ? $get_header_image : get_template_directory_uri() . '/assets/img/demo/blog-title-bg.png';
 ?>
<div class="page-heading" style="background:#333 url('<?php echo esc_url( $header_image ); ?> ')no-repeat; background-size:cover;">
	<div class="container">
		<div class="row">

			<div class="col-md-12">
				<?php if ( is_404() ) { ?>

					<h2 class="entry-title"><?php esc_html_e( 'Page Not Found', 'kabheen' ); ?></h2>

				<?php } elseif ( is_home() ) { 

					$title 		= esc_html__( 'Blog', 'kabheen' );
					$posts_page = get_option( 'page_for_posts' );
					if ( ! empty( $posts_page ) ) {
						$title = get_the_title( $posts_page );
					}
					?>
					<h2 class="entry-title"><?php echo esc_attr( $title ); ?></h2>

				<?php } elseif ( is_category() ) { ?>

					<h2 class="entry-title">
						<?php esc_html_e( 'Category : ', 'kabheen' ); ?><strong><?php single_cat_title(); ?></strong>
					</h2>

				<?php } elseif ( is_tag() ) { ?>

					<h2 class="entry-title">
						<?php esc_html_e( 'Tag : ', 'kabheen' ); ?><strong><?php single_tag_title(); ?></strong>
					</h2>

				<?php } elseif ( is_tax() ) { ?>

					<h2 class="entry-title"><?php single_term_title(); ?></h2>

				<?php } elseif ( is_author() ) { ?>

					<?php $author_name = get_the_author_meta( 'display_name', get_query_var( 'author' ) ); ?>
					<h2 class="entry-title fn n">
						<?php esc_html_e( 'Author : ', 'kabheen' ); ?><strong><?php echo ''.$author_name; ?></strong>
					</h2>

				<?php } elseif ( is_search() ) { ?>

					<h2 class="entry-title"><?php echo esc_attr( get_search_query() ); ?></h2>

				<?php } elseif ( is_day() || is_month() || is_year() ) { ?>

					<?php
						if ( is_day() )
							$date = get_the_time( esc_html__( 'F d, Y', 'kabheen' ) );
						elseif ( is_month() )
							$date = get_the_time( esc_html__( 'F Y', 'kabheen' ) );
						elseif ( is_year() )
							$date = get_the_time( esc_html__( 'Y', 'kabheen' ) );
					?>

					<h2 class="entry-title"><?php printf( $date ); ?></h2>

				<?php } elseif ( class_exists( 'WooCommerce' ) && is_shop() ) { ?>

					<h2 class="entry-title"><?php esc_html_e( 'Shop', 'kabheen' ); ?></h2>

				<?php } elseif ( is_archive() ) { ?>

					<?php the_archive_title( '<h2 class="entry-title">', '</h1>' ); ?>

				<?php } elseif ( is_singular( 'post' ) ) { ?>

					<h2 class="entry-title"><?php esc_html_e( 'Blog', 'kabheen' ); ?></h2>

				<?php } elseif ( is_singular( 'product' ) ) { ?>

					<h2 class="entry-title"><?php esc_html_e( 'Shop', 'kabheen' ); ?></h2>

				<?php } elseif ( is_page() ) { ?>

					<h2 class="entry-title"><?php single_post_title(); ?></h2>

				<?php } elseif ( is_singular( 'jogja-portfolio' ) ) { ?>

					<h2 class="entry-title"><?php echo apply_filters( 'oilumeo_single_portfolio_page_title', esc_html__( 'Portfolios', 'kabheen' ) ); ?></h2>

				<?php } // End if check  ?>

			</div><!-- end col -->
		</div><!-- end row -->
	</div><!-- end container -->
</div><!-- end featured-slider -->