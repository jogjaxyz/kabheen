<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="entry-format">
		<?php kabheen_entry_meta(); ?>
	</div><!-- end entry-format -->

	<div class="inner">
		<?php if ( has_post_thumbnail() ) : ?>
			<div class="entry-media">
				<?php kabheen_get_the_image( 660, 500 ); ?>
			</div><!-- end entry-media -->
		<?php endif; ?>
		<?php if ( is_single() ) : ?>
			<h1 class="entry-title">
				<?php echo single_post_title(); ?>
			</h1>
		<?php else : ?>
			<h2 class="entry-title">
				<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
			</h2>
		<?php endif; ?>
		<div class="entry-header">
			<?php kabheen_post_author_name(); ?>
			<?php kabheen_post_categories(); ?>
			<?php kabheen_post_comment_link(); ?>
		</div><!-- end entry-header -->
		<div class="entry-content">
			<?php
				if ( is_single() ) :
					the_content();				
				else :
					the_excerpt();
				endif;
				
				wp_link_pages( array(
					'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'kabheen' ),
					'after'  => '</div>',
				) );
			?>
		</div><!-- end entry-content -->
	</div><!-- end inner -->
</div><!-- end post -->