<?php

/**
 * Template Name: Events
 *
 * The Template for page template events
 *
 * @author 		Kungfuthemes
 * @version     1.0
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

get_header(); ?>
	
	<?php if ( function_exists( 'carbon_get_post_meta' ) ) : ?>
		<?php get_template_part( 'template-parts/content-event' ); ?>
	<?php endif; ?>

<?php get_footer();