<?php

/**
 * Template Name: Galleries
 *
 * The Template for page template galleries
 *
 * @author 		Kungfuthemes
 * @version     1.0
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

get_header(); ?>
	
	<?php if ( function_exists( 'carbon_get_post_meta' ) ) : ?>
		<?php get_template_part( 'template-parts/content-gallery-template' ); ?>
	<?php endif; ?>

<?php get_footer();