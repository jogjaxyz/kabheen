<?php
/**
 * The template for displaying archive pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Jogja
 */

get_header(); ?>

	<!-- START MAIN CONTENT -->
	<div id="main">
		<div class="container"> 
			<div class="row">

				<div id="primary" class="content-area col-md-9">
					<div id="content" class="site-content">

						<?php
						if ( have_posts() ) : ?>

							<?php
							/* Start the Loop */
							while ( have_posts() ) : the_post();

								/*
								 * Include the Post-Format-specific template for the content.
								 * If you want to override this in a child theme, then include a file
								 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
								 */
								get_template_part( 'template-parts/format', get_post_format() );

							endwhile;

						else :

							get_template_part( 'template-parts/content', 'none' );

						endif; ?>
					</div>

					<?php the_posts_navigation(); ?>
					
				</div>

				<?php get_sidebar(); ?>

			</div>
		</div><!-- #main -->
	</div><!-- #primary -->

<?php get_footer();
