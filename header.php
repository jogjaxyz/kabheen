<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Jogja
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

	<header id="masterhead" class="site-header fixed-on">
		<div class="container">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".site-navigation" aria-expanded="false">
				<span class="sr-only"><?php esc_html_e( 'Toggle navigation', 'kabheen' ); ?></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<div class="row">
				<div class="col-md-12">
					<?php get_template_part( 'template-parts/menu-primary' ); ?>
				</div><!-- end col -->
			</div><!-- end row -->
		</div><!-- end container -->
	</header><!-- end header -->

	<?php get_template_part( 'template-parts/page-title' ); ?>


