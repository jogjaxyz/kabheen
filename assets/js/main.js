(function($) {

	'use strict';  

	jQuery(document).ready(function($) {

		// OWL SLIDER
		$(".kabheen-slider").each(function(){
			var dataNav 	= $(this).data('nav'),
				autoPlay 	= $(this).data('autoplay'),
				item 		= $(this).data('item'),
				loop 		= $(this).data('loop');
			
			$(this).owlCarousel({
				nav: dataNav,
				autoplay: autoPlay,
				autoplayTimeout: 5000,
				items: 1,
				loop: true,
			});
		});

		// COUNTDOWN TIMER
		$(".defaultCountdown").each(function(){
			var countdownDate = $(this).data('date');
			$(this).countdown(countdownDate, function(event) {
				$(this).html(event.strftime(
				'<span class="countdown-row countdown-show5">'
				+ '<span class="countdown-section"><span class="countdown-amount">%w</span> <span class="countdown-period">weeks</span></span>'
				+ '<span class="countdown-section"><span class="countdown-amount">%d</span> <span class="countdown-period">days</span></span>'
				+ '<span class="countdown-section"><span class="countdown-amount">%H</span> <span class="countdown-period">hours</span></span>'
				+ '<span class="countdown-section"><span class="countdown-amount">%M</span> <span class="countdown-period">minutes</span></span>'
				+ '<span class="countdown-section"><span class="countdown-amount">%S</span> <span class="countdown-period">seconds</span></span>'
				+ '</span>' ));
			});
		});
		
		
		// EQUAL HEIGHT
		function equalHeight(group) {
			var tallest = 0;
			group.each(function() {
				var thisHeight = $(this).height();
				if(thisHeight > tallest) {
					tallest = thisHeight;
				}
			});
			group.height(tallest);
		}
		
		equalHeight($(".equal"));

		// MASONRY
		$(".testimonial-content").masonry({
		  // options
		  itemSelector: '.testimonial-grid',
		  columnWidth: '.testimonial-grid',
		  percentPosition: true
		}); 

		// FILTERING CONTENT
		$("#gallery-grid").mixItUp();

		// PAGINATION HANDLE
		$( ".paging .nav-previous" ).addClass( "col-md-6" );
		$( ".paging .nav-previous a" ).addClass( "alignleft" );
		$( ".paging .nav-previous a" ).prepend( '<i class="fa fa-fw fa-caret-left"></i>' );
		$( ".paging .nav-next" ).addClass( "col-md-6" );
		$( ".paging .nav-next a" ).addClass( "alignright" );
		$( ".paging .nav-next a" ).append( '<i class="fa fa-fw fa-caret-right"></i>' );

		// SOCIAL SHARE BUTTON
		$(".other-share").each(function(){
			var title 	  = $(this).data("title"),
				text      = $(this).data("text"),
				image     = $(this).data("image"),
				url       = $(this).data("url"),
				width     = $(this).data("width"),
				height    = $(this).data("height");

			$(this).find("a").ShareLink({
				title: title,
				text: text,
				image: image,
				url: url,
				width: width,
				height: height
			});

		});

	   
	});         

})( jQuery );