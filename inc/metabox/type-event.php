<?php 

/**
 * Event Metabox
 *
 * @return void
 * @author 
 **/
add_filter( 'cs_metabox_options', 'kabheen_type_event_metabox' );
function kabheen_type_event_metabox( $options ) {

	// -----------------------------------------
	// Event Metabox Options                    -
	// -----------------------------------------
	$options[] 		= array(
		'id'        => 'kabheen_event_details',
		'title'     => esc_html__( 'Event Details', 'kabheen' ),
		'post_type' => 'kabheen-event',
		'context'   => 'normal',
		'priority'  => 'default',
		'sections'  => array(
			array(
				'name'   => 'section_event',
				'fields' => array(
					array(
						'id' 			=> 'date',
						'type' 			=> 'text',
						'title' 		=> esc_html__( 'Event Date', 'kabheen' ),
					),
					array(
						'id' 			=> 'time',
						'type' 			=> 'text',
						'title' 		=> esc_html__( 'Event Time', 'kabheen' ),
					),
					array(
						'id' 			=> 'venue',
						'type' 			=> 'text',
						'title' 		=> esc_html__( 'Event Venue', 'kabheen' ),
					),
					array(
						'id' 			=> 'map_link',
						'type' 			=> 'text',
						'title' 		=> esc_html__( 'Event Map Link', 'kabheen' ),
					),
				),
			),
		),
	);

	return $options;
}