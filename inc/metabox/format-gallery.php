<?php 

/**
 * Post Format Gallery Metabox
 *
 * @return void
 * @author 
 **/
add_filter( 'cs_metabox_options', 'kabheen_format_gallery_metabox' );
function kabheen_format_gallery_metabox( $options ) {

	$options 		= array();

	// -----------------------------------------
	// Post Metabox Options                    -
	// -----------------------------------------
	$options[] 		= array(
		'id'        => 'kabheen_format_gallery_details',
		'title'     => esc_html__( 'Gallery Details', 'kabheen' ),
		'post_type' => 'post',
		'context'   => 'normal',
		'priority'  => 'default',
		'sections'  => array(
			array(
				'name'   => 'section_gallery_items',
				'fields' => array(
					array(
						'id' 			=> 'items',
						'type' 			=> 'gallery',
						'title' 		=> esc_html__( 'Gallery Images', 'kabheen' ),
						'add_title' 	=> esc_html__( 'Add Images', 'kabheen' ),
						'edit_title'	=> esc_html__( 'Edit Images', 'kabheen' ),
						'clear_title' 	=> esc_html__( 'Remove Images', 'kabheen' ),
					),
				),
			),
		),
	);

	return $options;
}