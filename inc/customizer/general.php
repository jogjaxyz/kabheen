<?php

add_filter( 'cs_customize_options', 'kabheen_customizer_general' );
function kabheen_customizer_general( $options ) {

	// -----------------------------------------
	// Customize Panel Options Fields          -
	// -----------------------------------------
	$options 			= array();
	$options[]			= array(
		'name'			=> 'kabheen_customizer_general_panel',
		'title'			=> esc_html__( 'General Settings', 'kabheen' ),
		'sections'		=> array(
			array(
				'name'          => 'section_1',
				'title'         => 'Section 1',
				'settings'      => array(
					array(
						'name'      => 'color_option_1',
						'default'   => '#ffbc00',
						'control'   => array(
							'type'    => 'cs_field',
							'options' => array(
								'type'  => 'color_picker',
								'title' => 'Color Option 1',
							),
						),
					),

				array(
				'name'      => 'color_option_2',
				'default'   => '#2ecc71',
					'control'   => array(
						'type'    => 'cs_field',
						'options' => array(
							'type'  => 'color_picker',
							'title' => 'Color Option 2',
						),
					),
				),

			array(
			'name'      => 'color_option_3',
			'default'   => '#e74c3c',
			'control'   => array(
			'type'    => 'cs_field',
			'options' => array(
			'type'  => 'color_picker',
			'title' => 'Color Option 3',
			),
			),
			),

			array(
			'name'      => 'color_option_4',
			'default'   => '#3498db',
			'control'   => array(
			'type'    => 'cs_field',
			'options' => array(
			'type'  => 'color_picker',
			'title' => 'Color Option 4',
			),
			),
			),

			array(
			'name'      => 'color_option_5',
			'default'   => '#555555',
			'control'   => array(
			'type'    => 'cs_field',
			'options' => array(
			'type'  => 'color_picker',
			'title' => 'Color Option 5',
			),
			),
			),

			),
			),
			// end: section

		// begin: section
		array(
		'name'          => 'section_2',
		'title'         => 'Section 2',
		'settings'      => array(

		array(
		'name'      => 'text_option_1',
		'control'   => array(
		'type'    => 'cs_field',
		'options' => array(
		'type'  => 'text',
		'title' => 'Text Option 1',
		),
		),
		),

		array(
		'name'      => 'text_option_2',
		'control'   => array(
		'type'    => 'cs_field',
		'options' => array(
		'type'  => 'text',
		'title' => 'Text Option 2',
		),
		),
		),

		array(
		'name'      => 'text_option_3',
		'control'   => array(
		'type'    => 'cs_field',
		'options' => array(
		'type'  => 'text',
		'title' => 'Text Option 3',
		),
		),
		),

		),
		),
		// end: section

	),
	// end: sections

	);
	
	return $options;

}


/**
 * Modify Customizer section
 *
 * @return void
 * @author tokoo
 **/
add_action( 'customize_register', 'kabheen_modify_default_customizer_section' );
function kabheen_modify_default_customizer_section( $wp_customize ) {
	$wp_customize->remove_section( 'colors' );
	$wp_customize->remove_section( 'header_image' );
	$wp_customize->get_section( 'title_tagline' )->panel = 'kabheen_customizer_general_panel';
	$wp_customize->get_section( 'title_tagline' )->priority = 0;
	$wp_customize->get_control( 'blogdescription' )->priority = 2;
	$wp_customize->get_control( 'display_header_text' )->priority = 5;
	$wp_customize->get_control( 'blogname' )->priority = 4;
	$wp_customize->get_control( 'site_icon' )->priority = 6;

}