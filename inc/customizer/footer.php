<?php

// FOOTER PANEL
// Kabheen_Kirki::add_panel( 'kabheen_footer', array(
// 	'priority'    => 10,
// 	'title'       => esc_html__( 'Footer Setting', 'kabheen' ),
// 	'description' => esc_html__( 'My Description', 'kabheen' ),
// ) );

// 	// THE BRIDE AND GROOM
// 	Kabheen_Kirki::add_section( 'kabheen_the_bride_and_groom_section', array(
// 		'title'			=> esc_attr__( 'The Bride and Groom', 'kabheen' ),
// 		'priority'		=> 1,
// 		'panel'			=> 'kabheen_footer',
// 		'capability' 	=> 'edit_theme_options',
// 	) );

// 		Kabheen_Kirki::add_field( 'kabheen_the_bride_and_groom_section_title', array(
// 			'type'		=> 'text',
// 			'settings'	=> 'kabheen_the_bride_and_groom_section_title',
// 			'label'		=> esc_html__( 'Section Title', 'kabheen' ),
// 			'section'	=> 'kabheen_the_bride_and_groom_section',
// 			'priority'	=> 1,
// 		) );

// 		Kabheen_Kirki::add_field( 'kabheen_the_bride_name', array(
// 			'type'		=> 'text',
// 			'settings'	=> 'kabheen_the_bride_name',
// 			'label'		=> esc_html__( 'The Bride Name', 'kabheen' ),
// 			'section'	=> 'kabheen_the_bride_and_groom_section',
// 			'priority'	=> 1,
// 		) );

// 		Kabheen_Kirki::add_field( 'kabheen_the_groom_name', array(
// 			'type'		=> 'text',
// 			'settings'	=> 'kabheen_the_groom_name',
// 			'label'		=> esc_html__( 'The Groom Name', 'kabheen' ),
// 			'section'	=> 'kabheen_the_bride_and_groom_section',
// 			'priority'	=> 1,
// 		) );

// 	// THE FOOTER TEXT
// 	Kabheen_Kirki::add_section( 'kabheen_footer_text_section', array(
// 		'title'			=> esc_attr__( 'The Footer Text', 'kabheen' ),
// 		'priority'		=> 2,
// 		'panel'			=> 'kabheen_footer',
// 		'capability' 	=> 'edit_theme_options',
// 	) );

// 		Kabheen_Kirki::add_field( 'kabheen_the_footer_text', array(
// 			'type'		=> 'textarea',
// 			'settings'	=> 'kabheen_the_footer_text',
// 			'label'		=> esc_html__( 'The Footer Text', 'kabheen' ),
// 			'section'	=> 'kabheen_footer_text_section',
// 			'priority'	=> 1,
// 		) );


add_filter( 'cs_customize_options', 'kabheen_customizer_footer' );
function kabheen_customizer_footer( $options ) {

	// -----------------------------------------
	// Customize Panel Options Fields          -
	// -----------------------------------------
	$options[]			= array(
		'name'			=> 'kabheen_customizer_footer_panel',
		'title'			=> esc_html__( 'Footer Settings', 'kabheen' ),
		'sections'		=> array(
			// THE BRIDE AND GROOM
			array(
				'name'          => 'the_bride_and_groom',
				'title'         => esc_html__( 'The Bride and Groom', 'kabheen' ),
				'settings'      => array(
					array(
						'name'      => 'kabheen_the_bride_and_groom_section_title',
						'default'   => '',
						'control'   => array(
							'type'    => 'cs_field',
							'options' => array(
								'type'  => 'text',
								'title' => esc_html__( 'Section Title', 'kabheen' ),
							),
						),
					),
					array(
						'name'      => 'kabheen_the_bride_name',
						'default'   => '',
						'control'   => array(
							'type'    => 'cs_field',
							'options' => array(
								'type'  => 'text',
								'title' => esc_html__( 'The Bride Name', 'kabheen' ),
							),
						),
					),
					array(
						'name'      => 'kabheen_the_groom_name',
						'default'   => '',
						'control'   => array(
							'type'    => 'cs_field',
							'options' => array(
								'type'  => 'text',
								'title' => esc_html__( 'The Groom Name', 'kabheen' ),
							),
						),
					),
				),
			),

			// FOOTER TEXT SECTION
			array(
				'name'          => 'kabheen_footer_text_section',
				'title'         => esc_html__( 'Footer Text', 'kabheen' ),
				'settings'      => array(
					array(
						'name'      => 'kabheen_the_footer_text',
						'default'   => '',
						'control'   => array(
							'type'    => 'cs_field',
							'options' => array(
								'type'  => 'textarea',
								'title' => esc_html__( 'Footer Text', 'kabheen' ),
							),
						),
					),
				),
			),
		),
	);
	
	return $options;

}
