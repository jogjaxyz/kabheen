<?php


add_filter( 'cs_customize_options', 'kabheen_customizer_header' );
function kabheen_customizer_header( $options ) {

	// -----------------------------------------
	// Customize Panel Options Fields          -
	// -----------------------------------------
	$options[]			= array(
		'name'			=> 'kabheen_customizer_header_panel',
		'title'			=> esc_html__( 'Header Settings', 'kabheen' ),
		'sections'		=> array(
			// HEADER TYPE
			array(
				'name'          => 'header_settings',
				'title'         => esc_html__( 'Header Type', 'kabheen' ),
				'settings'      => array(
					array(
						'name'      => 'header_type',
						'default'   => '',
						'control'   => array(
							'type'    => 'cs_field',
							'options' => array(
								'type'  => 'text',
								'title' => esc_html__( 'Section Title', 'kabheen' ),
							),
						),
					),
					array(
						'name'      => 'tost',
						'default'   => '',
						'control'   => array(
							'type'    => 'cs_field',
							'options' => array(
								'type'  => 'text',
								'title' => esc_html__( 'The Bride Name', 'kabheen' ),
							),
						),
					),
					array(
						'name'      => 'kabheen_the_groom_name',
						'default'   => '',
						'control'   => array(
							'type'    => 'cs_field',
							'options' => array(
								'type'  => 'text',
								'title' => esc_html__( 'The Groom Name', 'kabheen' ),
							),
						),
					),
				),
			),

			// FOOTER TEXT SECTION
			array(
				'name'          => 'kabheen_footer_text_section',
				'title'         => esc_html__( 'Footer Text', 'kabheen' ),
				'settings'      => array(
					array(
						'name'      => 'kabheen_the_footer_text',
						'default'   => '',
						'control'   => array(
							'type'    => 'cs_field',
							'options' => array(
								'type'  => 'textarea',
								'title' => esc_html__( 'Footer Text', 'kabheen' ),
							),
						),
					),
				),
			),
		),
	);
	
	return $options;

}
