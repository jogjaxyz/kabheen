<?php 

/**
 * Get Font URL
 *
 * @return void
 * @author jogja
 **/
function kabheen_get_fonts_url() {
   
    $font_url = '';

    /*
    Translators: If there are characters in your language that are not supported
    by chosen font(s), translate this to 'off'. Do not translate into your own language.
     */
    if ( 'off' !== _x( 'on', 'Google font: on or off', 'kabheen' ) ) {
        $font_url = add_query_arg( 'family', urlencode( 'Oswald|Open+Sans:700,600' ), "//fonts.googleapis.com/css" );
    }
    return $font_url; 
}

/**
 * Enqueue scripts and styles.
 */
add_action( 'wp_enqueue_scripts', 'kabheen_scripts' );
function kabheen_scripts() {

	/* ================================================== *
	 * ENQUEUE STYLES 									  *
	 * ================================================== */
	wp_enqueue_style( 'kabheen-fonts', kabheen_get_fonts_url() );
	wp_enqueue_style( 'kabheen-style', get_stylesheet_uri() );
	wp_enqueue_style( 'bootstrap', get_template_directory_uri() . '/assets/css/bootstrap.min.css' );
	wp_enqueue_style( 'font-awesome', get_template_directory_uri() . '/assets/css/font-awesome.min.css' );
	wp_enqueue_style( 'owl-carousel', get_template_directory_uri() . '/assets/css/owl.carousel.min.css' );
	wp_enqueue_style( 'kabheen-plugins', get_template_directory_uri() . '/assets/css/plugins.min.css' );
	wp_enqueue_style( 'kabheen-main', get_template_directory_uri() . '/assets/css/style.css' );

	/* ================================================== *
	 * ENQUEUE SCRIPTS 									  *
	 * ================================================== */
	wp_enqueue_script( 'jquery-plugin', get_template_directory_uri() . '/assets/js/jquery.plugin.js', array( 'jquery' ), '', true );
	wp_enqueue_script( 'kabheen-plugins', get_template_directory_uri() . '/assets/js/plugins.min.js', array( 'jquery' ), '', true );
	wp_enqueue_script( 'kabheen-main', get_template_directory_uri() . '/assets/js/main.js', array( 'jquery' ), '', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}