<?php

if ( ! defined( 'ABSPATH' ) ) exit;


class KabheenElementor { 

	/**
	 * Constructor
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 */
	public function __construct() {
		$this->add_actions();
	}

	/**
	 * Add Actions
	 *
	 * @since 1.0.0
	 *
	 * @access private
	 */
	private function add_actions() {
		add_action( 'elementor/init', array( $this, 'add_elementor_category' ) );
		add_action( 'elementor/widgets/widgets_registered', [ $this, 'on_widgets_registered' ] );
	}

	/**
	 * On Widgets Registered
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 */
	public function add_elementor_category(){
		\Elementor\Plugin::instance()->elements_manager->add_category(
			'kabheen-elements',
			[
				'title' => esc_html__( 'Kabheen Elements', 'kabheen' ),
				'icon'  => 'font'
			],
			1
		);
	}

	/**
	 * On Widgets Registered
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 */
	public function on_widgets_registered() {
		$this->includes();
		$this->register_widget();
	}

	/**
	 * Includes
	 *
	 * @since 1.0.0
	 *
	 * @access private
	 */
	private function includes() {
		require_once get_template_directory() . '/inc/elementor/featured-slider.php';
		require_once get_template_directory() . '/inc/elementor/couple.php';
		require_once get_template_directory() . '/inc/elementor/countdown.php';
		require_once get_template_directory() . '/inc/elementor/stories.php';
		require_once get_template_directory() . '/inc/elementor/peoples.php';
		require_once get_template_directory() . '/inc/elementor/guest-book-slider.php';
		require_once get_template_directory() . '/inc/elementor/guest-book-grid.php';
		require_once get_template_directory() . '/inc/elementor/blog-lists.php';
		require_once get_template_directory() . '/inc/elementor/gift-images.php';
		
	}

	/**
	 * Register Widget
	 *
	 * @since 1.0.0
	 *
	 * @access private
	 */
	private function register_widget() {
		// Featured Slider
		\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new Elementor\KabheenFeaturedSlider() );
		\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new Elementor\KabheenCouple() );
		\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new Elementor\KabheenCountdown() );
		\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new Elementor\KabheenStories() );
		\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new Elementor\KabheenPeoples() );
		\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new Elementor\KabheenGuestBookSlider() );
		\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new Elementor\KabheenGuestBookGrid() );
		\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new Elementor\KabheenBlogLists() );
		\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new Elementor\KabheenGiftImages() );
		
	}

}

new KabheenElementor();