<?php 

/**
 * Ngaa Core Compatibility
 *
 * @return void
 * @author ngaa
 **/
add_action( 'init', 'kabheen_ngaa_setup', 10 );
function kabheen_ngaa_setup() {

	if ( function_exists( 'ngaa_type' ) ) {
		ngaa_type( 'kabheen-gallery',
			array(
				'admin_cols' => array(
					'title' 			=> array(
						'title'			=> esc_html__( ' Gallery Title', 'kabheen' ),
					),
					'featured_image' 	=> array(
						'title'          	=> esc_html__( 'Gallery Image', 'kabheen' ),
						'featured_image' 	=> 'thumbnail',
						'height' 			=> 80,
						'width' 			=> 80
					),
					'date'
				),
				
				'supports'      		=> array( 'title', 'thumbnail', 'editor' ),
				'menu_icon' 			=> 'dashicons-format-gallery',
				'menu_position' 		=> 7.25,
				'has_archive'			=> false,
				'public' 				=> false,  
				'publicly_queryable' 	=> true,  
				'show_ui' 				=> true,
				'exclude_from_search' 	=> true, 
				'show_in_nav_menus' 	=> false,
				'rewrite' 				=> false,
			),
			array(
				'singular' 				=> esc_html__( 'Gallery', 'kabheen' ),
				'plural'   				=> esc_html__( 'Galleries', 'kabheen' ),
				'slug'     				=> 'kb-gallery',
			)
		);
		
		ngaa_taxo( 'gallery_cat' , 'kabheen-gallery', array(),
			array(
				'singular' 	=> 'Category',
				'plural' 	=> 'Categories',
				'slug'		=> 'gallery-cat',
				)
		);

		ngaa_type( 'kabheen-event',
			array(
				'admin_cols' => array(
					'title' 			=> array(
						'title'			=> esc_html__( 'Event Title', 'kabheen' ),
					),
					'featured_image' 	=> array(
						'title'          	=> esc_html__( 'Event Image', 'kabheen' ),
						'featured_image' 	=> 'thumbnail',
						'height' 			=> 80,
						'width' 			=> 80
					),
					'date'
				),
				
				'supports'      		=> array( 'title', 'editor' ),
				'menu_icon' 			=> 'dashicons-calendar',
				'menu_position' 		=> 7.26,
				'has_archive'			=> false,
				'public' 				=> false,  
				'publicly_queryable' 	=> true,  
				'show_ui' 				=> true,
				'exclude_from_search' 	=> true, 
				'show_in_nav_menus' 	=> false,
				'rewrite' 				=> false,
			),
			array(
				'singular' 				=> esc_html__( 'Event', 'kabheen' ),
				'plural'   				=> esc_html__( 'Events', 'kabheen' ),
				'slug'     				=> 'kb-event',
			)
		);

		
	}

}