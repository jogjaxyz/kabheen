<?php

/**
 * Display post author name
 *
 * @return void
 * @author Kungfuthemes
 **/
function kabheen_post_author_name() {
	global $post; 
	$author_id = $post->post_author;
	?>
	<span class="author vcard">
		<?php esc_html_e( 'Posted by', 'kabheen' ); ?> <a href="<?php echo esc_url( get_author_posts_url( get_the_author_meta( 'ID', $author_id ) ) ); ?>"><?php echo ucfirst( get_the_author_meta( 'display_name', $author_id ) ); ?></a>
	</span><!-- end author -->
	<?php 
}

/**
 * Post categories
 *
 * @return void
 * @author Kungfuthemes
 **/
function kabheen_post_categories( $taxonomy = 'category' ) {
	$taxes 			= wp_get_object_terms( get_the_ID(), $taxonomy ); 
	$i 				= 0;
	$cats_length 	= count( $taxes ); 
	if ( ! empty( $taxes ) ) {
		echo '<span class="cat-links">';
			foreach ( $taxes as $cat ) { 
				$separator = ( $i !== $cats_length - 1 ) ? ', ' : ''; ?>
				<a href="<?php echo get_term_link( $cat->slug, $taxonomy ); ?>"><?php echo esc_attr( $cat->name ); ?></a>
			<?php $i++;
		}
		echo '</span>'; 
	}
}

/**
 * Post taxonomy custom id
 *
 * @return void
 * @author Kungfuthemes
 **/
function kabheen_post_taxonomies( $id = '', $taxonomy = 'category', $link = false ) {
	$taxes 			= wp_get_object_terms( $id, $taxonomy ); 
	$i 				= 0;
	$cats_length 	= count( $taxes ); 
	if ( ! empty( $taxes ) ) {
		foreach ( $taxes as $cat ) { 
			$separator = ( $i !== $cats_length - 1 ) ? ', ' : ''; 
			if ( true == $link ) {
				echo '<a href="'.get_term_link( $cat->slug, $taxonomy ).'">';
			}

			echo esc_attr( $cat->name ) . $separator;

			if ( true == $link ) {
				echo '</a>';
			}
			$i++;
		}
	}
}

/**
 * Post tags
 *
 * @return void
 * @author Kungfuthemes
 **/
function kabheen_post_tags() {
	the_tags();
}

/**
 * Post Comment Link
 *
 * @return void
 * @author Kungfuthemes
 **/
function kabheen_post_comment_link() {
	$number    = (int) get_comments_number( get_the_ID() );
	if ( 0 < $number ) : 
	?>
		<div class="comment-link">
			<i class="fa fa-fw fa-comments"></i>
			<?php comments_popup_link( 'No comments yet', '1 comment', '% comments', 'comments-link', 'Comments are off for this post' ); ?>
		</div>
	<?php 
	endif;
}

/**
 * Post Date
 *
 * @return void
 * @author Kungfuthemes
 **/
function kabheen_post_date() {
	?>
	<div class="entry-date">
		<a href="<?php the_permalink(); ?>">
			<time class="published" datetime="<?php echo get_the_time( 'Y-m-d\TH:i:sP', get_the_ID() ); ?>" title="<?php echo get_the_time( esc_html__( 'l, F jS, Y, g:i a', 'kabheen' ), get_the_ID() ); ?>">
				<?php echo get_the_time( 'M' ); ?>
				<span class="date"><?php echo get_the_time( 'd' ); ?></span>
				<?php echo get_the_time( 'Y' ); ?>
			</time>
		</a>
	</div><!-- end entry-date -->
	<?php 
}

/**
 * Post social shares
 *
 * @return void
 * @author Kungfuthemes
 **/
function kabheen_post_social_shares() {
	global $post;

	if ( has_post_thumbnail() ) {
		$image 		= wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), 'large', false );
		$the_image 	= 'data-image="'.$image[0].'"';
	} else {
		$the_image 	= '';
	}
	?>
	<div class="social-shares">
		<span class="shrd"><?php esc_html_e( 'SHARE THIS', 'kabheen' ); ?></span>
		<div class="other-share" data-url="<?php the_permalink(); ?>" data-title="<?php the_title(); ?>" data-text="<?php echo strip_tags( $post->post_excerpt ); ?>" <?php echo ''.$the_image; ?> data-width="600" data-height="400">
			<a href="" class="facebook s_facebook"><i class="fa fa-fw fa-facebook"></i></a>
			<a href="" class="twitter s_twitter"><i class="fa fa-fw fa-twitter"></i></a>
			<a href="" class="google-plus s_plus"><i class="fa fa-fw fa-google-plus"></i></a>
			<a href="" class="linkedin s_linkedin"><i class="fa fa-fw fa-linkedin"></i></a>
		</div>
	</div>
	<?php 
}

/**
 * Entry meta
 *
 * @return void
 * @author Kungfuthemes
 **/
function kabheen_entry_meta() {
	?>
	<div class="entry-meta">
		<?php kabheen_post_date(); ?>
		<?php kabheen_post_social_shares(); ?>
	</div><!-- end entry-meta -->
	<?php 
}

/**
 * Display post image
 *
 * @return void
 * @author Kungfuthemes
 **/
function kabheen_get_the_image( $width = 300, $height = 300, $class = '' ) {
	$image_id 		= get_post_thumbnail_id( get_the_ID() );
	$image 			= wp_get_attachment_image_src( $image_id, 'full', false );
	$image_source 	= kabheen_image_resize( $image[0], $width, $height );
	$img_class 		= ! empty( $class ) ? 'class="'.$class.'"' : '';
	?>
		<img <?php echo esc_attr( $img_class ); ?>src="<?php echo esc_url( $image_source ); ?>" alt="<?php esc_html_e( 'Post image', 'kabheen' ); ?>">
	<?php
}

/**
 * Post about author
 *
 * @return void
 * @author Kungfuthemes
 **/
function kabheen_post_about_author() {
	global $post;
	$author_id = $post->post_author;
	?>
	<div class="single-author text-center">
		<div class="single-author-ava">
			<?php echo get_avatar( get_the_author_meta( 'user_email', $author_id ), 60 ); ?>                       
		</div>
		<h3 class="single-author-name"><?php echo get_the_author_meta( 'display_name', $author_id ); ?></h3>
		<?php echo wpautop( get_the_author_meta( 'description', get_query_var( 'author' ) ) ); ?>
		<p class="single-author-socmed">
			<?php if ( function_exists( 'carbon_get_user_meta' ) ) : ?>
				<a href="<?php echo get_the_author_meta( 'ID' ); ?>"><i class="fa fa-facebook"></i></a>
				<a href="<?php echo get_the_author_meta( 'ID' ); ?>"><i class="fa fa-twitter"></i></a>
				<a href="<?php echo get_the_author_meta( 'ID' ); ?>"><i class="fa fa-instagram"></i></a>
				<a href="<?php echo get_the_author_meta( 'ID' ); ?>"><i class="fa fa-pinterest"></i></a>
			<?php endif; ?>
		</p>
	</div>
	<?php
}

/**
 * Post comment callback
 *
 * @return void
 * @author Kungfuthemes
 **/
function kabheen_comment_callback( $comment, $args, $depth ) {
	$GLOBALS['comment'] = $comment;

	if ( 'pingback' == $comment->comment_type || 'trackback' == $comment->comment_type ) : ?>

	<li id="comment-<?php comment_ID(); ?>" <?php comment_class(); ?>>
		<div class="comment-body">
			<?php esc_html_e( 'Pingback:', 'kabheen' ); ?> <?php comment_author_link(); ?> <?php edit_comment_link( esc_html__( 'Edit', 'kabheen' ), '<span class="edit-link">', '</span>' ); ?>
		</div>

	<?php else : ?>

	<li id="comment-<?php comment_ID(); ?>" <?php comment_class( empty( $args['has_children'] ) ? '' : 'parent' ); ?>>

		<div id="div-comment-<?php comment_ID(); ?>" class="comment-body">

			<figure class="comment-author vcard">
				<?php if ( 0 != $args['avatar_size'] ) { echo get_avatar( $comment, $args['avatar_size'] ); } ?>
			</figure><!-- .comment-author -->
			
			<div class="comment-wrapper">
				<div class="comment-meta">
					<a href="<?php echo get_comment_author_link(); ?>"><b class="fn"><?php comment_author(); ?></b></a>
					<span class="comment-metadata">
						<a href="<?php echo esc_url( get_comment_link( $comment->comment_ID ) ); ?>">
							<?php printf( _x( '%1$s', '1: date', 'kabheen' ), get_comment_date() ); ?>
						</a>
					</span>
					<div class="reply">
						<?php comment_reply_link( array_merge( $args, array( 'reply_text' => esc_html__( 'Reply', 'kabheen' ), 'after' => ' <span>&darr;</span>', 'depth' => $depth, 'max_depth' => $args['max_depth'] ) ) ); ?>
					</div><!-- .reply -->
					<?php edit_comment_link( esc_html__( 'Edit', 'kabheen' ), '<span class="edit-link">', '</span>' ); ?>
				</div>
				<div class="comment-content">
					<?php if ( '0' == $comment->comment_approved ) : ?>
					<p class="comment-awaiting-moderation"><?php esc_html_e( 'Your comment is awaiting moderation.', 'kabheen' ); ?></p>
					<?php endif; ?>

					<?php comment_text(); ?>
				</div><!-- .comment-content -->
			</div>
		</div>
	</li>

	<?php
	endif;
}

 /**
  * Related Post
  *
  * @return void
  * @author 
  **/
function kabheen_related_post() {

	$include_categories = wp_get_object_terms( get_the_ID(), 'category', array( 'fields' => 'ids' ) );

	if ( empty( $exclude_categories ) ) {
		$exclude_categories = array();
	}

	if ( empty( $exclude_tags ) ) {
		$exclude_tags = array();
	}

	$args = array(
		'post_type' 		=> 'post',
		'post_status' 		=> 'publish',
		'posts_per_page' 	=> 3,
		'order' 			=> 'rand',
		'orderby' 			=> 'date',
		'category__in' 		=> $include_categories,
		'category__not_in'  => $exclude_categories,
		'post__not_in' 		=> array( get_the_ID() ) );


	$related_items = new WP_Query( $args ); ?>

	<?php if ( $related_items->have_posts() ) : ?>

		<div class="related-post">
			<h3 class="related-post-title"><?php esc_html_e( 'Related Posts', 'kabheen' ); ?></h3>
			<div class="row">

				<?php while ( $related_items->have_posts() ) : $related_items->the_post(); ?>

					<div class="related-post-item col-md-4 col-sm-4">
						<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
						<div class="entry-meta">
							<p>
								<?php esc_html_e( 'Posted by', 'kabheen' ); ?> <a href="<?php echo esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ); ?>"><?php echo ucfirst( get_the_author_meta( 'display_name' ) ); ?></a>
								<br><span class="entry-date"><?php echo get_the_time( 'F d, Y' ); ?></span>
							</p>
						</div>
					</div><!-- end related-post-item -->

				<?php endwhile;  ?>
				<?php wp_reset_postdata(); ?>

			</div>
		</div><!-- .related -->

	<?php endif;

}
