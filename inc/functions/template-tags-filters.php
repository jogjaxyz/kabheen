<?php 

/**
 * Override pagination markup
 *
 * @return void
 * @author Ngaamen
 **/
add_filter( 'navigation_markup_template', 'kabheen_override_pagination_markup', 10, 2 );
function kabheen_override_pagination_markup( $template, $class ) {
	$template = '
	<div class="paging %1$s" role="navigation">
		<div class="row">%3$s</div>
	</div>';

	return $template;
}


/**
 * Litle override wp_list_categories HTML output
 *
 * @return void
 * @author Ngaamen
 **/
add_filter( 'wp_list_categories', 'kabheen_cat_count_span' );
function kabheen_cat_count_span( $links ) {
	$links = str_replace( '<a href="', '<a class="pull-left" href="', $links );
	$links = str_replace( '</a> (', '</a> <span class="pull-right">', $links );
	$links = str_replace( ')', '</span>', $links );
	return $links;
}

/**
 * Override default template searchform
 *
 * @return void
 * @author Ngaamen
 **/
add_filter( 'get_search_form', 'kabheen_override_searchform' );
function kabheen_override_searchform( $form ) {
	ob_start();
	?>
	<div class="searchform">
		<form action="<?php echo esc_url( home_url( '/' ) ); ?>">
			<input type="text" name="s" class="txt" placeholder="<?php esc_attr_e( 'Search &hellip;', 'kabheen' ); ?>">
			<input type="submit" class="btn btn-sm" value="<?php esc_attr_e( 'Search', 'kabheen' ); ?>">
		</form>
	</div>
	<?php 
	$form = ob_get_contents();
	ob_get_clean();

	return $form;
}