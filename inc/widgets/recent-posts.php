<?php

if ( ! class_exists( 'Carbon_Fields\Widget' ) ) 
	return;

use Carbon_Fields\Widget;
use Carbon_Fields\Field;

class Kabheen_Widget_Recent_Posts extends Widget {

	function __construct() {
        $this->setup( esc_html__( 'Kabheen - Recent Posts', 'kabheen' ),  esc_html__( 'Displays the recent posts', 'kabheen' ), array(
            Field::make( 'text', 'title', esc_html__( 'Title', 'kabheen' ) )->set_default_value( 'Recent Posts' ),
            Field::make( 'text', 'limit', esc_html__( 'Limit', 'kabheen' ) )->set_default_value(4),
        ), 'post-type-widget' );
    }

    // Called when rendering the widget in the front-end
    function front_end( $args, $instance ) {
        $title 		= apply_filters( 'widget_title', $instance['title'] );
		printf( '%s', $args['before_widget'] );

		if ( $title ) {
			printf( '%s %s %s', $args['before_title'], $title, $args['after_title'] );
		}

		$post_args = array( 
			'post_type' 		=> 'post',
			'posts_per_page'	=> ! empty( $instance['limit'] ) ? $instance['limit'] : 4,
			'orderby'           => 'date',
        	'order'             => 'DESC'
		);
		$recent_posts = new WP_Query( $post_args );
		?>
		<ul>
			<?php if ( $recent_posts->have_posts() ) : ?>

				<?php while ( $recent_posts->have_posts() ) : $recent_posts->the_post(); ?>

					<li>
						<figure class="post-thumbnail">
							<a href="<?php the_permalink(); ?>">
								<?php if ( has_post_thumbnail() ) : ?>
									<?php $img_src = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), 'full' ); ?>
									<img src="<?php echo kabheen_image_resize( $img_src[0], 265, 170 ); ?>">
								<?php else : ?>
									<img src="http://placehold.it/265x170" alt="Placeholder">
								<?php endif; ?>
							</a>
						</figure>
						<span class="post-category">
							<?php echo get_the_time( 'd M Y' ); ?>
						</span>
						<h2 class="post-title">
							<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
						</h2>
					</li>
			
				<?php endwhile; ?>
				<?php wp_reset_postdata(); ?>
				
				<?php else: ?>
					<li><h2 class="post-title"><?php esc_html_e( 'You have no posts to displayed', 'kabheen' ); ?></h2></li>
			<?php endif; ?>
		</ul>
		
		<?php 
		printf( '%s', $args['after_widget'] );
    }

}

