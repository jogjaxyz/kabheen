<?php

namespace Elementor;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class KabheenGuestBookSlider extends Widget_Base {

	public function get_name() {
		return 'kabheen-guest-book-slider';
	}

	public function get_title() {
		return esc_html__( 'Kabheen Guest Book Slider', 'kabheen' );
	}

	public function get_icon() {
		return 'fa fa-newspaper-o';
	}

	public function get_categories() {
		return [ 'kabheen-elements' ];
	}

	protected function _register_controls() {
		// HEADING
		$this->start_controls_section(
			'section_heading',
			[
				'label' => esc_html__( 'Guest Book Content', 'kabheen' ),
			]
		);

			$this->add_control(
				'section_title',
				[
					'label' 	=> esc_html__( 'Section Title', 'kabheen' ),
					'type' 		=> Controls_Manager::TEXT,
					'default' 	=> 'The Groomsmen',
					'title' 	=> esc_html__( 'Enter some text', 'kabheen' ),
				]
			);

			$this->add_control(
				'guest_book_items',
				[
					'label' 	=> esc_html__( 'Guest Book Items', 'kabheen' ),
					'type' 		=> Controls_Manager::REPEATER,
					'fields' 	=> [
						[
							'name' 		=> 'name',
							'label' 	=> esc_html__( 'Name', 'kabheen' ),
							'type' 		=> Controls_Manager::TEXT,
							'default' 	=> '',
						],
						[
							'name' 		=> 'content',
							'label' 	=> esc_html__( 'Content', 'kabheen' ),
							'type' 		=> Controls_Manager::TEXTAREA,
							'default' 	=> '',
						],
						[
							'name' 		=> 'image',
							'label' 	=> esc_html__( 'Image', 'kabheen' ),
							'type' 		=> Controls_Manager::MEDIA,
							'default'	=> [
								'url'	=> ''
							]
						],
					],
				]
			);
		$this->end_controls_section();

		$this->start_controls_section(
			'section_style', 
			[
				'label' => esc_html__( 'Styles', 'kabheen' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);
			
			$this->add_control(
				'bride_color',
				[
					'label' 	=> esc_html__( 'Bride Color', 'kabheen' ),
					'type' 		=> Controls_Manager::COLOR,
					'value' 	=> '',
					'selectors' => [
						'{{WRAPPER}} .caption h1' => 'color: {{VALUE}}',
					],
				]
			);
			$this->add_control(
				'bride_border_color',
				[
					'label' 	=> esc_html__( 'Bride Border Color', 'kabheen' ),
					'type' 		=> Controls_Manager::COLOR,
					'value' 	=> '',
					'selectors' => [
						'{{WRAPPER}} .caption h1' => 'border-bottom-color: {{VALUE}}',
					],
				]
			);
			$this->add_control(
				'description_color',
				[
					'label' 	=> esc_html__( 'Description Color', 'kabheen' ),
					'type' 		=> Controls_Manager::COLOR,
					'value' 	=> '',
					'selectors' => [
						'{{WRAPPER}} .caption p' => 'color: {{VALUE}}',
					],
				]
			);
			$this->add_control(
				'bride_font_size',
				[
					'label' 	=> esc_html__( 'Bride Font Size', 'kabheen' ),
					'type' 		=> Controls_Manager::SLIDER,
					'default' 	=> [
						'size' 	=> '',
					],
					'range' => [
						'px' => [
							'min' => 0,
							'max' => 200,
							'step' => 1,
						],
					],
					'size_units' 	=> [ 'px', '%' ],
					'selectors' 	=> [
						'{{WRAPPER}} .caption h1' => 'font-size: {{SIZE}}{{UNIT}};',
					],
				]
			);
			$this->add_control(
				'description_font_size',
				[
					'label' 	=> esc_html__( 'Description Font Size', 'kabheen' ),
					'type' 		=> Controls_Manager::SLIDER,
					'default' 	=> [
						'size' 	=> '',
					],
					'range' => [
						'px' => [
							'min' => 0,
							'max' => 200,
							'step' => 1,
						],
					],
					'size_units' 	=> [ 'px', '%' ],
					'selectors' 	=> [
						'{{WRAPPER}} .caption p' => 'font-size: {{SIZE}}{{UNIT}};',
					],
				]
			);

		$this->end_controls_section();

	}

	protected function render() {

		$instance = $this->get_settings();
		?>
			<div class="guest-book">
				<div class="owl-text kabheen-slider" data-nav="true" data-autoplay="false">

					<?php if ( ! empty( $instance['guest_book_items'] ) ) : ?>
						<?php foreach ( $instance['guest_book_items'] as $item ) : ?>
							<div>
								<p><?php echo ''.$item['content']; ?></p>
								<span><?php echo ''.$item['name']; ?></span>
								<?php if ( ! empty( $item['image']['url'] ) ) : ?>
									<figure class="vatar">
										<img src="<?php echo kabheen_image_resize( $item['image']['url'], 80, 80 ); ?>" alt="">
									</figure>
								<?php endif; ?>
							</div>
						<?php endforeach; ?>
					<?php endif; ?>

				</div><!-- end owl-text -->
			</div>

		<?php 
	}
}

