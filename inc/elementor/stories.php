<?php

namespace Elementor;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class KabheenStories extends Widget_Base {

	public function get_name() {
		return 'kabheen-stories';
	}

	public function get_title() {
		return esc_html__( 'Kabheen Stories', 'kabheen' );
	}

	public function get_icon() {
		return 'fa fa-newspaper-o';
	}

	public function get_categories() {
		return [ 'kabheen-elements' ];
	}

	protected function _register_controls() {
		// HEADING
		$this->start_controls_section(
			'section_heading',
			[
				'label' => esc_html__( 'Stories Content', 'kabheen' ),
			]
		);

			$this->add_control(
				'story_items',
				[
					'label' 	=> esc_html__( 'Story Items', 'kabheen' ),
					'type' 		=> Controls_Manager::REPEATER,
					'fields' 	=> [
						[
							'name' 		=> 'date',
							'label' 	=> esc_html__( 'Story Date', 'kabheen' ),
							'type' 		=> Controls_Manager::TEXT,
							'default' 	=> '',
						],
						[
							'name' 		=> 'title',
							'label' 	=> esc_html__( 'Story Title', 'kabheen' ),
							'type' 		=> Controls_Manager::TEXT,
							'default' 	=> '',
						],
						[
							'name' 		=> 'content',
							'label' 	=> esc_html__( 'Story Content', 'kabheen' ),
							'type' 		=> Controls_Manager::WYSIWYG,
							'default' 	=> '',
						],
					],
				]
			);
		$this->end_controls_section();

		$this->start_controls_section(
			'section_style', 
			[
				'label' => esc_html__( 'Styles', 'kabheen' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);
			
			$this->add_control(
				'bride_color',
				[
					'label' 	=> esc_html__( 'Bride Color', 'kabheen' ),
					'type' 		=> Controls_Manager::COLOR,
					'value' 	=> '',
					'selectors' => [
						'{{WRAPPER}} .caption h1' => 'color: {{VALUE}}',
					],
				]
			);
			$this->add_control(
				'bride_border_color',
				[
					'label' 	=> esc_html__( 'Bride Border Color', 'kabheen' ),
					'type' 		=> Controls_Manager::COLOR,
					'value' 	=> '',
					'selectors' => [
						'{{WRAPPER}} .caption h1' => 'border-bottom-color: {{VALUE}}',
					],
				]
			);
			$this->add_control(
				'description_color',
				[
					'label' 	=> esc_html__( 'Description Color', 'kabheen' ),
					'type' 		=> Controls_Manager::COLOR,
					'value' 	=> '',
					'selectors' => [
						'{{WRAPPER}} .caption p' => 'color: {{VALUE}}',
					],
				]
			);
			$this->add_control(
				'bride_font_size',
				[
					'label' 	=> esc_html__( 'Bride Font Size', 'kabheen' ),
					'type' 		=> Controls_Manager::SLIDER,
					'default' 	=> [
						'size' 	=> '',
					],
					'range' => [
						'px' => [
							'min' => 0,
							'max' => 200,
							'step' => 1,
						],
					],
					'size_units' 	=> [ 'px', '%' ],
					'selectors' 	=> [
						'{{WRAPPER}} .caption h1' => 'font-size: {{SIZE}}{{UNIT}};',
					],
				]
			);
			$this->add_control(
				'description_font_size',
				[
					'label' 	=> esc_html__( 'Description Font Size', 'kabheen' ),
					'type' 		=> Controls_Manager::SLIDER,
					'default' 	=> [
						'size' 	=> '',
					],
					'range' => [
						'px' => [
							'min' => 0,
							'max' => 200,
							'step' => 1,
						],
					],
					'size_units' 	=> [ 'px', '%' ],
					'selectors' 	=> [
						'{{WRAPPER}} .caption p' => 'font-size: {{SIZE}}{{UNIT}};',
					],
				]
			);

		$this->end_controls_section();

	}

	protected function render() {

		$instance = $this->get_settings();

		?>
			<?php if ( ! empty( $instance['story_items'] ) ) : ?>
				
				<div class="story-inner equal">
					
					<?php foreach ( $instance['story_items'] as $item ) : ?>
						
						<div class="line-story ">
							<?php if ( ! empty( $item['date'] ) ) : ?>
								<span class="date-line"><?php echo ''.$item['date']; ?></span>
							<?php endif; ?>
							<h2><?php echo ''.$item['title']; ?></h2>
							<p><?php echo ''.$item['content']; ?></p>
						</div><!-- end line-story -->

					<?php endforeach; ?>

				</div><!-- end story-inner -->

			<?php endif; ?>
		<?php 

	}

}

