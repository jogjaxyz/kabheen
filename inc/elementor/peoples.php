<?php

namespace Elementor;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class KabheenPeoples extends Widget_Base {

	public function get_name() {
		return 'kabheen-peoples';
	}

	public function get_title() {
		return esc_html__( 'Kabheen Peoples', 'kabheen' );
	}

	public function get_icon() {
		return 'fa fa-newspaper-o';
	}

	public function get_categories() {
		return [ 'kabheen-elements' ];
	}

	protected function _register_controls() {
		// HEADING
		$this->start_controls_section(
			'section_heading',
			[
				'label' => esc_html__( 'Peoples Content', 'kabheen' ),
			]
		);

			$this->add_control(
				'section_title',
				[
					'label' 	=> esc_html__( 'Section Title', 'kabheen' ),
					'type' 		=> Controls_Manager::TEXT,
					'default' 	=> 'The Groomsmen',
					'title' 	=> esc_html__( 'Enter some text', 'kabheen' ),
				]
			);

			$this->add_control(
				'peoples_type',
				[
					'label' 	=> esc_html__( 'Peoples Type', 'kabheen' ),
					'type' 		=> Controls_Manager::TEXT,
					'default' 	=> 'groomsmen',
					'title' 	=> esc_html__( 'Select the type', 'kabheen' ),
					'options'	=> [
						'groomsmen' 	=> esc_html__( 'The Groomsmen', 'kabheen' ),
						'bridesmaid' 	=> esc_html__( 'The Bridesmaid', 'kabheen' ),
					]
				]
			);

			$this->add_control(
				'peoples_items',
				[
					'label' 	=> esc_html__( 'Peoples Items', 'kabheen' ),
					'type' 		=> Controls_Manager::REPEATER,
					'fields' 	=> [
						[
							'name' 		=> 'name',
							'label' 	=> esc_html__( 'People Name', 'kabheen' ),
							'type' 		=> Controls_Manager::TEXT,
							'default' 	=> '',
						],
						[
							'name' 		=> 'image',
							'label' 	=> esc_html__( 'Image', 'kabheen' ),
							'type' 		=> Controls_Manager::MEDIA,
							'default'	=> [
								'url'	=> ''
							]
						],
					],
				]
			);
		$this->end_controls_section();

		$this->start_controls_section(
			'section_style', 
			[
				'label' => esc_html__( 'Styles', 'kabheen' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);
			
			$this->add_control(
				'bride_color',
				[
					'label' 	=> esc_html__( 'Bride Color', 'kabheen' ),
					'type' 		=> Controls_Manager::COLOR,
					'value' 	=> '',
					'selectors' => [
						'{{WRAPPER}} .caption h1' => 'color: {{VALUE}}',
					],
				]
			);
			$this->add_control(
				'bride_border_color',
				[
					'label' 	=> esc_html__( 'Bride Border Color', 'kabheen' ),
					'type' 		=> Controls_Manager::COLOR,
					'value' 	=> '',
					'selectors' => [
						'{{WRAPPER}} .caption h1' => 'border-bottom-color: {{VALUE}}',
					],
				]
			);
			$this->add_control(
				'description_color',
				[
					'label' 	=> esc_html__( 'Description Color', 'kabheen' ),
					'type' 		=> Controls_Manager::COLOR,
					'value' 	=> '',
					'selectors' => [
						'{{WRAPPER}} .caption p' => 'color: {{VALUE}}',
					],
				]
			);
			$this->add_control(
				'bride_font_size',
				[
					'label' 	=> esc_html__( 'Bride Font Size', 'kabheen' ),
					'type' 		=> Controls_Manager::SLIDER,
					'default' 	=> [
						'size' 	=> '',
					],
					'range' => [
						'px' => [
							'min' => 0,
							'max' => 200,
							'step' => 1,
						],
					],
					'size_units' 	=> [ 'px', '%' ],
					'selectors' 	=> [
						'{{WRAPPER}} .caption h1' => 'font-size: {{SIZE}}{{UNIT}};',
					],
				]
			);
			$this->add_control(
				'description_font_size',
				[
					'label' 	=> esc_html__( 'Description Font Size', 'kabheen' ),
					'type' 		=> Controls_Manager::SLIDER,
					'default' 	=> [
						'size' 	=> '',
					],
					'range' => [
						'px' => [
							'min' => 0,
							'max' => 200,
							'step' => 1,
						],
					],
					'size_units' 	=> [ 'px', '%' ],
					'selectors' 	=> [
						'{{WRAPPER}} .caption p' => 'font-size: {{SIZE}}{{UNIT}};',
					],
				]
			);

		$this->end_controls_section();

	}

	protected function render() {

		$instance = $this->get_settings();
		?>
			<div class="<?php echo esc_attr( $instance['peoples_type'] ); ?>-side">
				<?php if ( ! empty( $item['section_title'] ) ) : ?>
					<h3><?php echo esc_attr( $item['section_title'] ); ?></h3>
				<?php endif; ?>

				<ul>
					<?php if ( ! empty( $instance['peoples_items'] ) ) : ?>
						<?php foreach ( $instance['peoples_items'] as $item ) : ?>
							
							<li>
								<?php if ( ! empty( $item['image']['url'] ) ) : ?>
									<figure>
										<img src="<?php echo kabheen_image_resize( $item['image']['url'], 150, 150 ); ?>">
									</figure>
								<?php endif; ?>
								
								<?php if ( ! empty( $item['name'] ) ) : ?>
									<p class="tag-name"><?php echo ''.$item['name']; ?></p>
								<?php endif; ?>
							</li>

						<?php endforeach; ?>
					<?php endif; ?>
				</ul>
			</div><!-- end groomsmen-side -->

		<?php 
	}
}

