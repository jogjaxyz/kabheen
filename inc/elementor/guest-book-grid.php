<?php

namespace Elementor;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class KabheenGuestBookGrid extends Widget_Base {

	public function get_name() {
		return 'kabheen-guest-book-grid';
	}

	public function get_title() {
		return esc_html__( 'Kabheen Guest Book Grid', 'kabheen' );
	}

	public function get_icon() {
		return 'fa fa-newspaper-o';
	}

	public function get_categories() {
		return [ 'kabheen-elements' ];
	}

	protected function _register_controls() {
		// HEADING
		$this->start_controls_section(
			'section_heading',
			[
				'label' => esc_html__( 'Guest Book Content', 'kabheen' ),
			]
		);

			$this->add_control(
				'name',
				[
					'label' 	=> esc_html__( 'Name', 'kabheen' ),
					'type' 		=> Controls_Manager::TEXT,
					'default' 	=> 'The Groomsmen',
					'title' 	=> esc_html__( 'Enter some text', 'kabheen' ),
				]
			);
			$this->add_control(
				'content',
				[
					'label' 	=> esc_html__( 'Content', 'kabheen' ),
					'type' 		=> Controls_Manager::TEXTAREA,
					'default' 	=> '',
					'title' 	=> esc_html__( 'Enter some text', 'kabheen' ),
				]
			);

		$this->end_controls_section();

		$this->start_controls_section(
			'section_style', 
			[
				'label' => esc_html__( 'Styles', 'kabheen' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);
			
			$this->add_control(
				'bride_color',
				[
					'label' 	=> esc_html__( 'Bride Color', 'kabheen' ),
					'type' 		=> Controls_Manager::COLOR,
					'value' 	=> '',
					'selectors' => [
						'{{WRAPPER}} .caption h1' => 'color: {{VALUE}}',
					],
				]
			);
			$this->add_control(
				'bride_border_color',
				[
					'label' 	=> esc_html__( 'Bride Border Color', 'kabheen' ),
					'type' 		=> Controls_Manager::COLOR,
					'value' 	=> '',
					'selectors' => [
						'{{WRAPPER}} .caption h1' => 'border-bottom-color: {{VALUE}}',
					],
				]
			);
			$this->add_control(
				'description_color',
				[
					'label' 	=> esc_html__( 'Description Color', 'kabheen' ),
					'type' 		=> Controls_Manager::COLOR,
					'value' 	=> '',
					'selectors' => [
						'{{WRAPPER}} .caption p' => 'color: {{VALUE}}',
					],
				]
			);
			$this->add_control(
				'bride_font_size',
				[
					'label' 	=> esc_html__( 'Bride Font Size', 'kabheen' ),
					'type' 		=> Controls_Manager::SLIDER,
					'default' 	=> [
						'size' 	=> '',
					],
					'range' => [
						'px' => [
							'min' => 0,
							'max' => 200,
							'step' => 1,
						],
					],
					'size_units' 	=> [ 'px', '%' ],
					'selectors' 	=> [
						'{{WRAPPER}} .caption h1' => 'font-size: {{SIZE}}{{UNIT}};',
					],
				]
			);
			$this->add_control(
				'description_font_size',
				[
					'label' 	=> esc_html__( 'Description Font Size', 'kabheen' ),
					'type' 		=> Controls_Manager::SLIDER,
					'default' 	=> [
						'size' 	=> '',
					],
					'range' => [
						'px' => [
							'min' => 0,
							'max' => 200,
							'step' => 1,
						],
					],
					'size_units' 	=> [ 'px', '%' ],
					'selectors' 	=> [
						'{{WRAPPER}} .caption p' => 'font-size: {{SIZE}}{{UNIT}};',
					],
				]
			);

		$this->end_controls_section();

	}

	protected function render() {

		$instance = $this->get_settings();
		?>
			<div class="testimonial-grid">
				<blockquote>" <?php echo ''.$instance['content']; ?> " <cite><?php echo ''.$item['name']; ?></cite></blockquote>
			</div><!-- end column -->

		<?php 
	}
}

