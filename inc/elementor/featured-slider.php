<?php

namespace Elementor;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class KabheenFeaturedSlider extends Widget_Base {

	public function get_name() {
		return 'kabheen-featured-slider';
	}

	public function get_title() {
		return esc_html__( 'Kabheen Featured Slider', 'kabheen' );
	}

	public function get_icon() {
		return 'fa fa-newspaper-o';
	}

	public function get_categories() {
		return [ 'kabheen-elements' ];
	}

	protected function _register_controls() {
		// HEADING
		$this->start_controls_section(
			'section_heading',
			[
				'label' => esc_html__( 'Slide Content', 'kabheen' ),
			]
		);

			$this->add_control(
				'the_bride_name',
				[
					'label' 	=> esc_html__( 'The Bride Name', 'multipress' ),
					'type' 		=> Controls_Manager::TEXT,
					'default' 	=> 'Andien',
					'title' 	=> esc_html__( 'Enter some text', 'multipress' ),
				]
			);
			$this->add_control(
				'the_groom_name',
				[
					'label' 	=> esc_html__( 'The Groom Name', 'multipress' ),
					'type' 		=> Controls_Manager::TEXT,
					'default' 	=> 'Johny',
					'title' 	=> esc_html__( 'Enter some text', 'multipress' ),
				]
			);
			$this->add_control(
				'description',
				[
					'label' 	=> esc_html__( 'Description', 'multipress' ),
					'type' 		=> Controls_Manager::TEXTAREA,
					'default' 	=> 'JULY 24TH, 2017. SARINA BUILDING JAKARTA',
					'title' 	=> esc_html__( 'Enter some text', 'multipress' ),
				]
			);
			$this->add_control(
				'slide_images',
				[
					'label' 	=> esc_html__( 'Slide Images', 'kabheen' ),
					'type' 		=> Controls_Manager::REPEATER,
					'fields' 	=> [
						[
							'name' 		=> 'image',
							'label' 	=> esc_html__( 'Image', 'kabheen' ),
							'type' 		=> Controls_Manager::MEDIA,
							'default' 	=> '',
						],
					],
				]
			);
		$this->end_controls_section();

		$this->start_controls_section(
			'section_style', 
			[
				'label' => esc_html__( 'Styles', 'kabheen' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);
			
			$this->add_control(
				'bride_color',
				[
					'label' 	=> esc_html__( 'Bride Color', 'kabheen' ),
					'type' 		=> Controls_Manager::COLOR,
					'value' 	=> '',
					'selectors' => [
						'{{WRAPPER}} .caption h1' => 'color: {{VALUE}}',
					],
				]
			);
			$this->add_control(
				'bride_border_color',
				[
					'label' 	=> esc_html__( 'Bride Border Color', 'kabheen' ),
					'type' 		=> Controls_Manager::COLOR,
					'value' 	=> '',
					'selectors' => [
						'{{WRAPPER}} .caption h1' => 'border-bottom-color: {{VALUE}}',
					],
				]
			);
			$this->add_control(
				'description_color',
				[
					'label' 	=> esc_html__( 'Description Color', 'kabheen' ),
					'type' 		=> Controls_Manager::COLOR,
					'value' 	=> '',
					'selectors' => [
						'{{WRAPPER}} .caption p' => 'color: {{VALUE}}',
					],
				]
			);
			$this->add_control(
				'bride_font_size',
				[
					'label' 	=> esc_html__( 'Bride Font Size', 'kabheen' ),
					'type' 		=> Controls_Manager::SLIDER,
					'default' 	=> [
						'size' 	=> '',
					],
					'range' => [
						'px' => [
							'min' => 0,
							'max' => 200,
							'step' => 1,
						],
					],
					'size_units' 	=> [ 'px', '%' ],
					'selectors' 	=> [
						'{{WRAPPER}} .caption h1' => 'font-size: {{SIZE}}{{UNIT}};',
					],
				]
			);
			$this->add_control(
				'description_font_size',
				[
					'label' 	=> esc_html__( 'Description Font Size', 'kabheen' ),
					'type' 		=> Controls_Manager::SLIDER,
					'default' 	=> [
						'size' 	=> '',
					],
					'range' => [
						'px' => [
							'min' => 0,
							'max' => 200,
							'step' => 1,
						],
					],
					'size_units' 	=> [ 'px', '%' ],
					'selectors' 	=> [
						'{{WRAPPER}} .caption p' => 'font-size: {{SIZE}}{{UNIT}};',
					],
				]
			);

		$this->end_controls_section();

	}

	protected function render() {

		$instance = $this->get_settings();

		?>
			<section class="featured-slider">
				<div class="container-fluid">
					<div class="row">
						
						<?php if ( ! empty( $instance['slide_images'] ) ) : ?>
							<div class="owl-carousel kabheen-slider" data-autoplay="true" data-item="1" data-nav="false" data-loop="true">
								<?php foreach ( $instance['slide_images'] as $item ) : ?>
									<div class="img-wrapper">
										<img src="<?php echo kabheen_image_resize( $item['image']['url'], 1440, 700 ); ?>" alt="Image 1">
									</div>
								<?php endforeach; ?>
							</div><!-- end owl-carousel -->
						<?php endif; ?>

						<div class="caption">
							<?php if ( ! empty( $instance['the_bride_name'] ) || ! empty( $instance['the_groom_name'] ) ) : ?>
								<h1><?php echo esc_attr( $instance['the_bride_name'] ); ?> &amp; <?php echo esc_attr( $instance['the_groom_name'] ); ?></h1>
							<?php endif; ?>
							<?php if ( ! empty( $instance['description'] ) ) : ?>
								<p><?php echo ''.$instance['description']; ?></p>
							<?php endif; ?>
						</div><!-- end caption -->

					</div><!-- end row -->
				</div><!-- end container -->
			</section><!-- end featured-slider -->
		<?php 

	}

}

