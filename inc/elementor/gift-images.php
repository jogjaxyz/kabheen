<?php

namespace Elementor;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class KabheenGiftImages extends Widget_Base {

	public function get_name() {
		return 'kabheen-gift-images';
	}

	public function get_title() {
		return esc_html__( 'Kabheen Gift Images', 'kabheen' );
	}

	public function get_icon() {
		return 'fa fa-newspaper-o';
	}

	public function get_categories() {
		return [ 'kabheen-elements' ];
	}

	protected function _register_controls() {
		// HEADING
		$this->start_controls_section(
			'section_heading',
			[
				'label' => esc_html__( 'Gift Content', 'kabheen' ),
			]
		);

			$this->add_control(
				'gift_items',
				[
					'label' 	=> esc_html__( 'Gift Items', 'kabheen' ),
					'type' 		=> Controls_Manager::REPEATER,
					'fields' 	=> [
						[
							'name' 		=> 'image',
							'label' 	=> esc_html__( 'Image', 'kabheen' ),
							'type' 		=> Controls_Manager::MEDIA,
							'default'	=> [
								'url'	=> ''
							]
						],
						[
							'name' 		=> 'link',
							'label' 	=> esc_html__( 'Link', 'kabheen' ),
							'type' 		=> Controls_Manager::TEXT,
							'default' 	=> '#',
						],
					],
				]
			);
		$this->end_controls_section();

	}

	protected function render() {

		$instance = $this->get_settings();
		?>
			<ul>
				<?php if ( ! empty( $instance['gift_items'] ) ) : ?>
					<?php foreach ( $instance['gift_items'] as $item ) : ?>
						
						<li>
							<a href="<?php echo ''.$item['link']; ?>">
								<img src="<?php echo esc_url( $item['image']['url'] ); ?>" alt="logo">
							</a>
						</li>

					<?php endforeach; ?>
				<?php endif; ?>
			</ul>

		<?php 
	}
}

