<?php

namespace Elementor;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class KabheenBlogLists extends Widget_Base {

	public function get_name() {
		return 'kabheen-blog-lists';
	}

	public function get_title() {
		return esc_html__( 'Kabheen Blog Lists', 'kabheen' );
	}

	public function get_icon() {
		return 'fa fa-newspaper-o';
	}

	public function get_categories() {
		return [ 'kabheen-elements' ];
	}

	public function get_posts( $type = 'post' ) {

	$posts = get_posts( array(
		'posts_per_page' 	=> -1,
		'post_type'			=> $type,
	));

	$result 	= array();
	$result[0]	= esc_html__( 'Select Item', 'pustaka' );
	foreach ( $posts as $post )	{
		$result[$post->ID] = $post->post_title;
	}
	return $result;
}

	protected function _register_controls() {
		// HEADING
		$this->start_controls_section(
			'section_heading',
			[
				'label' => esc_html__( 'Blog Lists Content', 'kabheen' ),
			]
		);

			$this->add_control(
				'post_items',
				[
					'label' 	=> esc_html__( 'Select Posts', 'kabheen' ),
					'type' 		=> Controls_Manager::SELECT2,
					'default' 	=> '',
					'title' 	=> esc_html__( 'Select the posts item', 'kabheen' ),
					'multiple'  => true,
					'options'	=> $this->get_posts(),
				]
			);

		$this->end_controls_section();

		$this->start_controls_section(
			'section_style', 
			[
				'label' => esc_html__( 'Styles', 'kabheen' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);
			
			$this->add_control(
				'bride_color',
				[
					'label' 	=> esc_html__( 'Bride Color', 'kabheen' ),
					'type' 		=> Controls_Manager::COLOR,
					'value' 	=> '',
					'selectors' => [
						'{{WRAPPER}} .caption h1' => 'color: {{VALUE}}',
					],
				]
			);
			$this->add_control(
				'bride_border_color',
				[
					'label' 	=> esc_html__( 'Bride Border Color', 'kabheen' ),
					'type' 		=> Controls_Manager::COLOR,
					'value' 	=> '',
					'selectors' => [
						'{{WRAPPER}} .caption h1' => 'border-bottom-color: {{VALUE}}',
					],
				]
			);
			$this->add_control(
				'description_color',
				[
					'label' 	=> esc_html__( 'Description Color', 'kabheen' ),
					'type' 		=> Controls_Manager::COLOR,
					'value' 	=> '',
					'selectors' => [
						'{{WRAPPER}} .caption p' => 'color: {{VALUE}}',
					],
				]
			);
			$this->add_control(
				'bride_font_size',
				[
					'label' 	=> esc_html__( 'Bride Font Size', 'kabheen' ),
					'type' 		=> Controls_Manager::SLIDER,
					'default' 	=> [
						'size' 	=> '',
					],
					'range' => [
						'px' => [
							'min' => 0,
							'max' => 200,
							'step' => 1,
						],
					],
					'size_units' 	=> [ 'px', '%' ],
					'selectors' 	=> [
						'{{WRAPPER}} .caption h1' => 'font-size: {{SIZE}}{{UNIT}};',
					],
				]
			);
			$this->add_control(
				'description_font_size',
				[
					'label' 	=> esc_html__( 'Description Font Size', 'kabheen' ),
					'type' 		=> Controls_Manager::SLIDER,
					'default' 	=> [
						'size' 	=> '',
					],
					'range' => [
						'px' => [
							'min' => 0,
							'max' => 200,
							'step' => 1,
						],
					],
					'size_units' 	=> [ 'px', '%' ],
					'selectors' 	=> [
						'{{WRAPPER}} .caption p' => 'font-size: {{SIZE}}{{UNIT}};',
					],
				]
			);

		$this->end_controls_section();

	}

	protected function render() {

		$instance = $this->get_settings();
		?>
			<?php if ( ! empty( $instance['post_items'] ) ) : ?>
				
				<div class="latest-blog-list">
						
					<?php 
						$args = [
							'posts_per_page' 		=> 3,
							'ignore_sticky_posts'	=> true,
							'post__in'				=> $instance['post_items'],
						]; 
						$latest_posts = new \WP_Query( $args );

					?>	
					<?php if ( $latest_posts->have_posts() ) : while ( $latest_posts->have_posts() ) : $latest_posts->the_post(); ?>
						
						<div class="col-sm-4">
							<a href="<?php the_permalink(); ?>" class="title"><?php the_title(); ?></a>
							<div class="meta">
								<p>
									<?php esc_html_e( 'Posted By:', 'kabheen' ); ?> 
									<?php the_author_posts_link(); ?>
								</p>
								<p>
									<?php esc_html_e( 'Categories:', 'kabheen' ); ?> 
									<?php kabheen_post_taxonomies( get_the_ID(), 'category', true ); ?>
								</p>
							</div><!-- end meta -->
						</div><!-- end col -->

					<?php endwhile; ?>
					<?php wp_reset_postdata(); ?>
					<?php endif; ?>
					

					
				</div><!-- end latest-blog-list -->

			<?php endif; ?>
		<?php 
	}
}

