<?php

namespace Elementor;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class KabheenEvents extends Widget_Base {

	public function get_name() {
		return 'kabheen-events';
	}

	public function get_title() {
		return esc_html__( 'Kabheen Events', 'kabheen' );
	}

	public function get_icon() {
		return 'fa fa-newspaper-o';
	}

	public function get_categories() {
		return [ 'kabheen-elements' ];
	}

	public function get_posts( $type = 'kabheen-event' ) {

	$posts = get_posts( array(
		'posts_per_page' 	=> -1,
		'post_type'			=> $type,
	));

	$result 	= array();
	$result[0]	= esc_html__( 'Select Item', 'pustaka' );
	foreach ( $posts as $post )	{
		$result[$post->ID] = $post->post_title;
	}
	return $result;
}

	protected function _register_controls() {
		// HEADING
		$this->start_controls_section(
			'section_heading',
			[
				'label' => esc_html__( 'Events Content', 'kabheen' ),
			]
		);

			$this->add_control(
				'event_items',
				[
					'label' 	=> esc_html__( 'Select Events', 'kabheen' ),
					'type' 		=> Controls_Manager::SELECT2,
					'default' 	=> '',
					'title' 	=> esc_html__( 'Select the events item', 'kabheen' ),
					'multiple'  => true,
					'options'	=> $this->get_posts(),
				]
			);

		$this->end_controls_section();

		$this->start_controls_section(
			'section_style', 
			[
				'label' => esc_html__( 'Styles', 'kabheen' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);
			
			$this->add_control(
				'bride_color',
				[
					'label' 	=> esc_html__( 'Bride Color', 'kabheen' ),
					'type' 		=> Controls_Manager::COLOR,
					'value' 	=> '',
					'selectors' => [
						'{{WRAPPER}} .caption h1' => 'color: {{VALUE}}',
					],
				]
			);
			$this->add_control(
				'bride_border_color',
				[
					'label' 	=> esc_html__( 'Bride Border Color', 'kabheen' ),
					'type' 		=> Controls_Manager::COLOR,
					'value' 	=> '',
					'selectors' => [
						'{{WRAPPER}} .caption h1' => 'border-bottom-color: {{VALUE}}',
					],
				]
			);
			$this->add_control(
				'description_color',
				[
					'label' 	=> esc_html__( 'Description Color', 'kabheen' ),
					'type' 		=> Controls_Manager::COLOR,
					'value' 	=> '',
					'selectors' => [
						'{{WRAPPER}} .caption p' => 'color: {{VALUE}}',
					],
				]
			);
			$this->add_control(
				'bride_font_size',
				[
					'label' 	=> esc_html__( 'Bride Font Size', 'kabheen' ),
					'type' 		=> Controls_Manager::SLIDER,
					'default' 	=> [
						'size' 	=> '',
					],
					'range' => [
						'px' => [
							'min' => 0,
							'max' => 200,
							'step' => 1,
						],
					],
					'size_units' 	=> [ 'px', '%' ],
					'selectors' 	=> [
						'{{WRAPPER}} .caption h1' => 'font-size: {{SIZE}}{{UNIT}};',
					],
				]
			);
			$this->add_control(
				'description_font_size',
				[
					'label' 	=> esc_html__( 'Description Font Size', 'kabheen' ),
					'type' 		=> Controls_Manager::SLIDER,
					'default' 	=> [
						'size' 	=> '',
					],
					'range' => [
						'px' => [
							'min' => 0,
							'max' => 200,
							'step' => 1,
						],
					],
					'size_units' 	=> [ 'px', '%' ],
					'selectors' 	=> [
						'{{WRAPPER}} .caption p' => 'font-size: {{SIZE}}{{UNIT}};',
					],
				]
			);

		$this->end_controls_section();

	}

	protected function render() {

		$instance = $this->get_settings();
		?>
			<?php if ( ! empty( $instance['event_items'] ) ) : ?>
						
				<?php 
					$args = [
						'post_type'				=> 'kabheen-event',
						'posts_per_page' 		=> 3,
						'ignore_sticky_posts'	=> true,
						'post__in'				=> $instance['event_items'],
					]; 
					$latest_posts = new \WP_Query( $args );

				?>	
				<?php if ( $latest_posts->have_posts() ) : while ( $latest_posts->have_posts() ) : $latest_posts->the_post(); ?>

					<?php 
						$event_date 	= get_post_meta( get_the_ID(), 'kabheen_event_date' ); 
						$event_time 	= get_post_meta( get_the_ID(), 'kabheen_event_time' ); 
						$event_venue 	= get_post_meta( get_the_ID(), 'kabheen_event_venue' ); 
						$event_map_link = get_post_meta( get_the_ID(), 'kabheen_event_map_link' ); 
					?>
					
					<div class="event-box">
						<h3><?php the_title(); ?></h3>
						<?php if ( ! empty( $event_date ) ) : ?>
							<p class="date"><?php echo ''.$event_date; ?></p>
						<?php endif; ?>
						<?php if ( ! empty( $event_venue ) ) : ?>
							<p class="place"><?php echo ''.$event_venue; ?></p>
						<?php endif; ?>
						<?php if ( ! empty( $event_map_link ) ) : ?>
							<div class="map-link">
								<a href="<?php echo esc_url( $event_map_link ); ?>" target="blank"><i class="fa fa-map-marker"></i></a>
							</div>
						<?php endif; ?>
					</div><!-- end event-box -->

				<?php endwhile; ?>
				<?php wp_reset_postdata(); ?>
				<?php endif; ?>
					
			<?php endif; ?>
		<?php 
	}
}

