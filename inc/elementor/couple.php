<?php

namespace Elementor;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class KabheenCouple extends Widget_Base {

	public function get_name() {
		return 'kabheen-couple';
	}

	public function get_title() {
		return esc_html__( 'Kabheen Couple', 'kabheen' );
	}

	public function get_icon() {
		return 'fa fa-newspaper-o';
	}

	public function get_categories() {
		return [ 'kabheen-elements' ];
	}

	protected function _register_controls() {
		// HEADING
		$this->start_controls_section(
			'section_heading',
			[
				'label' => esc_html__( 'Couple Content', 'kabheen' ),
			]
		);

			$this->add_control(
				'couple_name',
				[
					'label' 	=> esc_html__( 'Couple Name', 'kabheen' ),
					'type' 		=> Controls_Manager::TEXT,
					'default' 	=> 'Andien Sulistiawati',
					'title' 	=> esc_html__( 'Enter some text', 'kabheen' ),
				]
			);
			$this->add_control(
				'couple_title',
				[
					'label' 	=> esc_html__( 'Couple Title', 'kabheen' ),
					'type' 		=> Controls_Manager::TEXT,
					'default' 	=> 'The Bride',
					'title' 	=> esc_html__( 'Enter some text', 'kabheen' ),
				]
			);
			$this->add_control(
				'content',
				[
					'label' 	=> esc_html__( 'Content', 'kabheen' ),
					'type' 		=> Controls_Manager::WYSIWYG,
					'default' 	=> 'Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum.',
					'title' 	=> esc_html__( 'Enter some text', 'kabheen' ),
				]
			);
			$this->add_control(
				'image',
				[
					'label' 	=> esc_html__( 'Image', 'kabheen' ),
					'type' 		=> Controls_Manager::MEDIA,
					'default'	=> [
						'url'	=> ''
					]
				]
			);
			$this->add_control(
				'facebook',
				[
					'label' 	=> esc_html__( 'Facebook URL', 'kabheen' ),
					'type' 		=> Controls_Manager::TEXT,
					'default' 	=> '',
					'title' 	=> esc_html__( 'Enter the URL', 'kabheen' ),
				]
			);
			$this->add_control(
				'twitter',
				[
					'label' 	=> esc_html__( 'Twitter URL', 'kabheen' ),
					'type' 		=> Controls_Manager::TEXT,
					'default' 	=> '',
					'title' 	=> esc_html__( 'Enter the URL', 'kabheen' ),
				]
			);
			$this->add_control(
				'instagram',
				[
					'label' 	=> esc_html__( 'Instagram URL', 'kabheen' ),
					'type' 		=> Controls_Manager::TEXT,
					'default' 	=> '',
					'title' 	=> esc_html__( 'Enter the URL', 'kabheen' ),
				]
			);
			$this->add_control(
				'pinterest',
				[
					'label' 	=> esc_html__( 'Pinterest URL', 'kabheen' ),
					'type' 		=> Controls_Manager::TEXT,
					'default' 	=> '',
					'title' 	=> esc_html__( 'Enter the URL', 'kabheen' ),
				]
			);
			
		$this->end_controls_section();

		$this->start_controls_section(
			'alignment_settings',
			[
				'label' => esc_html__( 'Alignment Settings', 'kabheen' ),
			]
		);
			$this->add_control(
				'content_alignment',
				[
					'type' 			=> Controls_Manager::SELECT,
					'label' 		=> esc_html__( 'Content Alignment', 'multipress' ),
					'default' 		=> 'onleft', 
					'options' 		=> [
						'onleft' 		=> esc_html__( 'Left', 'multipress' ),
						'onright' 	=> esc_html__( 'Right', 'multipress' ),
					],
				]
			);
		$this->end_controls_section();

		$this->start_controls_section(
			'section_style', 
			[
				'label' => esc_html__( 'Styles', 'kabheen' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);
			
			$this->add_control(
				'couple_name_color',
				[
					'label' 	=> esc_html__( 'Couple Name Color', 'kabheen' ),
					'type' 		=> Controls_Manager::COLOR,
					'value' 	=> '',
					'selectors' => [
						'{{WRAPPER}} h3' => 'color: {{VALUE}}',
					],
				]
			);
			$this->add_control(
				'couple_title_color',
				[
					'label' 	=> esc_html__( 'Couple Title Color', 'kabheen' ),
					'type' 		=> Controls_Manager::COLOR,
					'value' 	=> '',
					'selectors' => [
						'{{WRAPPER}} span' => 'color: {{VALUE}}',
					],
				]
			);
			$this->add_control(
				'content_color',
				[
					'label' 	=> esc_html__( 'Content Color', 'kabheen' ),
					'type' 		=> Controls_Manager::COLOR,
					'value' 	=> '',
					'selectors' => [
						'{{WRAPPER}} p' => 'color: {{VALUE}}',
					],
				]
			);
			$this->add_control(
				'social_icons_color',
				[
					'label' 	=> esc_html__( 'Social Icons Color', 'kabheen' ),
					'type' 		=> Controls_Manager::COLOR,
					'value' 	=> '',
					'selectors' => [
						'{{WRAPPER}} .social i' => 'color: {{VALUE}}',
					],
				]
			);
			$this->add_control(
				'couple_name_size',
				[
					'label' 	=> esc_html__( 'Couple Name Font Size', 'kabheen' ),
					'type' 		=> Controls_Manager::SLIDER,
					'default' 	=> [
						'size' 	=> '',
					],
					'range' => [
						'px' => [
							'min' => 0,
							'max' => 200,
							'step' => 1,
						],
					],
					'size_units' 	=> [ 'px', '%' ],
					'selectors' 	=> [
						'{{WRAPPER}} h3' => 'font-size: {{SIZE}}{{UNIT}};',
					],
				]
			);
			$this->add_control(
				'couple_title_size',
				[
					'label' 	=> esc_html__( 'Couple Title Font Size', 'kabheen' ),
					'type' 		=> Controls_Manager::SLIDER,
					'default' 	=> [
						'size' 	=> '',
					],
					'range' => [
						'px' => [
							'min' => 0,
							'max' => 200,
							'step' => 1,
						],
					],
					'size_units' 	=> [ 'px', '%' ],
					'selectors' 	=> [
						'{{WRAPPER}} span' => 'font-size: {{SIZE}}{{UNIT}};',
					],
				]
			);
			$this->add_control(
				'content_font_size',
				[
					'label' 	=> esc_html__( 'Content Font Size', 'kabheen' ),
					'type' 		=> Controls_Manager::SLIDER,
					'default' 	=> [
						'size' 	=> '',
					],
					'range' => [
						'px' => [
							'min' => 0,
							'max' => 200,
							'step' => 1,
						],
					],
					'size_units' 	=> [ 'px', '%' ],
					'selectors' 	=> [
						'{{WRAPPER}} p' => 'font-size: {{SIZE}}{{UNIT}};',
					],
				]
			);
			$this->add_control(
				'social_icons_font_size',
				[
					'label' 	=> esc_html__( 'Social Icons Font Size', 'kabheen' ),
					'type' 		=> Controls_Manager::SLIDER,
					'default' 	=> [
						'size' 	=> '',
					],
					'range' => [
						'px' => [
							'min' => 0,
							'max' => 200,
							'step' => 1,
						],
					],
					'size_units' 	=> [ 'px', '%' ],
					'selectors' 	=> [
						'{{WRAPPER}} .social i' => 'font-size: {{SIZE}}{{UNIT}};',
					],
				]
			);

		$this->end_controls_section();

	}

	protected function render() {

		$instance = $this->get_settings();
		?>
		<div class="couple">
			<div class="<?php echo ''.$instance['content_alignment']; ?>">	
				<?php if ( 'onright' == $instance['content_alignment'] ) : ?>
					<div class="col-md-8">
						<figure>
							<img src="<?php echo kabheen_image_resize( $instance['image']['url'], 730, 568 ); ?>" alt="bride">
						</figure>
					</div>
				<?php endif; ?>

				<div class="col-md-4">
					<?php if ( ! empty( $instance['couple_title'] ) ) : ?>
						<span><?php echo ''.$instance['couple_title'] ?></span>
					<?php endif; ?>
					
					<?php if ( ! empty( $instance['couple_name'] ) ) : ?>
						<h3><?php echo ''.$instance['couple_name']; ?></h3>
					<?php endif; ?>
					
					<?php if ( ! empty( $instance['content'] ) ) : ?>
						<?php echo ''.$instance['content']; ?>
					<?php endif; ?>

					<div class="social">
						<?php if ( ! empty( $instance['facebook'] ) ) : ?>
							<a href="<?php echo esc_url( $instance['facebook'] ); ?>" title="Facebook"><i class="fa fa-facebook"></i></a>
						<?php endif; ?>
						<?php if ( ! empty( $instance['twitter'] ) ) : ?>
							<a href="<?php echo esc_url( $instance['twitter'] ); ?>" title="Twitter"><i class="fa fa-twitter"></i></a>
						<?php endif; ?>
						<?php if ( ! empty( $instance['instagram'] ) ) : ?>
							<a href="<?php echo esc_url( $instance['instagram'] ); ?>" title="Instagram"><i class="fa fa-instagram"></i></a>
						<?php endif; ?>
						<?php if ( ! empty( $instance['pinterest'] ) ) : ?>
							<a href="<?php echo esc_url( $instance['pinterest'] ); ?>" title="pinterest"><i class="fa fa-pinterest"></i></a>
						<?php endif; ?>
					</div><!-- end social -->
				</div><!-- end col -->

				<?php if ( 'onleft' == $instance['content_alignment'] ) : ?>
					<div class="col-md-8">
						<figure>
							<img src="<?php echo kabheen_image_resize( $instance['image']['url'], 730, 568 ); ?>" alt="bride">
						</figure>
					</div>
				<?php endif; ?>
			</div>	
		</div>				
		<?php 
	}

}

