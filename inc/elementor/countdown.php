<?php

namespace Elementor;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class KabheenCountdown extends Widget_Base {

	public function get_name() {
		return 'kabheen-countdown';
	}

	public function get_title() {
		return esc_html__( 'Kabheen Countdown', 'kabheen' );
	}

	public function get_icon() {
		return 'fa fa-newspaper-o';
	}

	public function get_categories() {
		return [ 'kabheen-elements' ];
	}

	protected function _register_controls() {
		// HEADING
		$this->start_controls_section(
			'section_heading',
			[
				'label' => esc_html__( 'Countdown Content', 'kabheen' ),
			]
		);

			$this->add_control(
				'countdown_date',
				[
					'label' 	=> esc_html__( 'Coundown Date', 'kabheen' ),
					'type' 		=> Controls_Manager::TEXT,
					'title' 	=> esc_html__( 'Enter the data with the format yyyy/mm/dd', 'kabheen' ),
					'default' 	=> '2017/08/20',
				]
			);
			
		$this->end_controls_section();

		$this->start_controls_section(
			'section_style', 
			[
				'label' => esc_html__( 'Styles', 'kabheen' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);
			
			$this->add_control(
				'countdown_color',
				[
					'label' 	=> esc_html__( 'Countdown Color', 'kabheen' ),
					'type' 		=> Controls_Manager::COLOR,
					'value' 	=> '',
					'selectors' => [
						'{{WRAPPER}} .countdown-section span' => 'color: {{VALUE}}',
					],
				]
			);
			$this->add_control(
				'coundown_border_color',
				[
					'label' 	=> esc_html__( 'Countdown Border Color', 'kabheen' ),
					'type' 		=> Controls_Manager::COLOR,
					'value' 	=> '',
					'selectors' => [
						'{{WRAPPER}} .is-countdown' => 'border-color: {{VALUE}}',
					],
				]
			);
			
			$this->add_control(
				'countdown_font_size',
				[
					'label' 	=> esc_html__( 'Countdown Font Size', 'kabheen' ),
					'type' 		=> Controls_Manager::SLIDER,
					'default' 	=> [
						'size' 	=> '',
					],
					'range' => [
						'px' => [
							'min' => 0,
							'max' => 200,
							'step' => 1,
						],
					],
					'size_units' 	=> [ 'px', '%' ],
					'selectors' 	=> [
						'{{WRAPPER}} .countdown-section span' => 'font-size: {{SIZE}}{{UNIT}};',
					],
				]
			);
			
		$this->end_controls_section();

	}

	protected function render() {

		$instance = $this->get_settings();
		?>
			<div class="defaultCountdown is-countdown" data-date="<?php echo esc_attr( $instance['countdown_date'] ); ?>"></div>			
		<?php 
	}

}

