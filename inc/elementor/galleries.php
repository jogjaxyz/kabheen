<?php

namespace Elementor;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class KabheenGalleries extends Widget_Base {

	public function get_name() {
		return 'kabheen-galleries';
	}

	public function get_title() {
		return esc_html__( 'Kabheen Galleries', 'kabheen' );
	}

	public function get_icon() {
		return 'fa fa-newspaper-o';
	}

	public function get_categories() {
		return [ 'kabheen-elements' ];
	}

	public function get_posts( $type = 'kabheen-gallery' ) {

	$posts = get_posts( array(
		'posts_per_page' 	=> -1,
		'post_type'			=> $type,
	));

	$result 	= array();
	$result[0]	= esc_html__( 'Select Item', 'pustaka' );
	foreach ( $posts as $post )	{
		$result[$post->ID] = $post->post_title;
	}
	return $result;
}

	protected function _register_controls() {
		// HEADING
		$this->start_controls_section(
			'section_heading',
			[
				'label' => esc_html__( 'Gallery Content', 'kabheen' ),
			]
		);

			$this->add_control(
				'gallery_items',
				[
					'label' 	=> esc_html__( 'Select Galleries', 'kabheen' ),
					'type' 		=> Controls_Manager::SELECT2,
					'default' 	=> '',
					'title' 	=> esc_html__( 'Select the galleries item', 'kabheen' ),
					'multiple'  => true,
					'options'	=> $this->get_posts(),
				]
			);

		$this->end_controls_section();

		$this->start_controls_section(
			'section_style', 
			[
				'label' => esc_html__( 'Styles', 'kabheen' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);
			
			$this->add_control(
				'bride_color',
				[
					'label' 	=> esc_html__( 'Bride Color', 'kabheen' ),
					'type' 		=> Controls_Manager::COLOR,
					'value' 	=> '',
					'selectors' => [
						'{{WRAPPER}} .caption h1' => 'color: {{VALUE}}',
					],
				]
			);
			$this->add_control(
				'bride_border_color',
				[
					'label' 	=> esc_html__( 'Bride Border Color', 'kabheen' ),
					'type' 		=> Controls_Manager::COLOR,
					'value' 	=> '',
					'selectors' => [
						'{{WRAPPER}} .caption h1' => 'border-bottom-color: {{VALUE}}',
					],
				]
			);
			$this->add_control(
				'description_color',
				[
					'label' 	=> esc_html__( 'Description Color', 'kabheen' ),
					'type' 		=> Controls_Manager::COLOR,
					'value' 	=> '',
					'selectors' => [
						'{{WRAPPER}} .caption p' => 'color: {{VALUE}}',
					],
				]
			);
			$this->add_control(
				'bride_font_size',
				[
					'label' 	=> esc_html__( 'Bride Font Size', 'kabheen' ),
					'type' 		=> Controls_Manager::SLIDER,
					'default' 	=> [
						'size' 	=> '',
					],
					'range' => [
						'px' => [
							'min' => 0,
							'max' => 200,
							'step' => 1,
						],
					],
					'size_units' 	=> [ 'px', '%' ],
					'selectors' 	=> [
						'{{WRAPPER}} .caption h1' => 'font-size: {{SIZE}}{{UNIT}};',
					],
				]
			);
			$this->add_control(
				'description_font_size',
				[
					'label' 	=> esc_html__( 'Description Font Size', 'kabheen' ),
					'type' 		=> Controls_Manager::SLIDER,
					'default' 	=> [
						'size' 	=> '',
					],
					'range' => [
						'px' => [
							'min' => 0,
							'max' => 200,
							'step' => 1,
						],
					],
					'size_units' 	=> [ 'px', '%' ],
					'selectors' 	=> [
						'{{WRAPPER}} .caption p' => 'font-size: {{SIZE}}{{UNIT}};',
					],
				]
			);

		$this->end_controls_section();

	}

	protected function render() {

		$instance = $this->get_settings();
		?>
			<?php if ( ! empty( $instance['gallery_items'] ) ) : ?>
					
				<div class="thumbnail-wrapper">

				<?php 
					$args = [
						'post_type'				=> 'kabheen-gallery',
						'posts_per_page' 		=> 3,
						'ignore_sticky_posts'	=> true,
						'post__in'				=> $instance['gallery_items'],
					]; 
					$galleries = new \WP_Query( $args );

				?>	
				<?php if ( $galleries->have_posts() ) : while ( $galleries->have_posts() ) : $galleries->the_post(); ?>

					<?php 
						$classes 		= get_the_terms( get_the_ID(), 'gallery_cat' ); 
						$gallery_image	= wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), 'full' );
					?>
					<?php if ( $gallery_image[0] ) : ?>
						<figure class="mix col-md-3 col-sm-6 col-xs-12 thumb <?php foreach ( $classes as $class ) { echo esc_attr( $class->slug ) . ' '; } ?>">
							<a href="#" data-toggle="modal" data-target=".pop-up-<?php echo esc_attr( $item['gallery'] ); ?>">
								<img src="<?php echo kabheen_image_resize( $gallery_image[0], 262, 213 ); ?>">
							</a>
						</figure><!-- end col -->
					<?php endif; ?>
					
					<?php ob_start(); ?>
					
						<div class="modal fade pop-up-<?php echo esc_attr( $item['gallery'] ); ?>" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel-<?php echo esc_attr( $item['gallery'] ); ?>" aria-hidden="true">
							<div class="modal-dialog modal-lg">
								<div class="modal-content">
									<div class="modal-header">
										<h4 class="modal-title" id="myLargeModalLabel-<?php echo esc_attr( $item['gallery'] ); ?>"><?php echo get_the_title( $item['gallery'] ); ?></h4>
									</div>
									<div class="modal-body">
										<?php if ( $gallery_image[0] ) : ?>
											<img src="<?php echo esc_url( $gallery_image[0] ); ?>">
										<?php endif; ?>
									</div>
								</div><!-- end modal content -->
							</div><!-- end modal-dialog -->
						</div><!-- end modal -->
	 
					<?php 
						$popups[] = ob_get_contents();
						ob_end_clean(); 
					?>

				<?php endwhile; ?>
				<?php wp_reset_postdata(); ?>
				<?php endif; ?>

				</div>

				<?php if ( ! empty( $popups ) ) : ?>
					<?php foreach ( $popups as $popup ) : ?>
						<?php echo ''.$popup; ?>
					<?php endforeach; ?>
				<?php endif; ?>
					
			<?php endif; ?>

		<?php 
	}
}

