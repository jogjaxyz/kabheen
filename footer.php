<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Jogja
 */

?>

	<div class="bottom-space"></div>
	
	<div class="bottom-section">
		
		<?php //get_template_part( 'template-parts/bottom-section' ); ?>

		<footer class="site-credit">
			<div class="container">
				<?php $theme 		= wp_get_theme(); ?>
				<?php $footer_text 	= get_theme_mod( 'kabheen_the_footer_text' ); ?>
				<?php if ( ! empty( $footer_text ) ) : ?>
					<?php echo wpautop( $footer_text ); ?>
				<?php else : ?>
					<p><?php echo sprintf( esc_html__( 'copyright %s %s %s. All Rights Reserved.', 'kabheen' ), '&copy;', date( 'Y' ), $theme->get( 'Name' ) ); ?></p>
				<?php endif; ?>
			</div>
		</footer>
	</div><!-- end bottom section -->

<?php wp_footer(); ?>
</body>
</html>
